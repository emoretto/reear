package br.eng.moretto.earmaker.gl3d;
import java.awt.Frame;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.nio.FloatBuffer;

import javax.media.opengl.DebugGLES3;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLCapabilities;
import javax.media.opengl.GLES3;
import javax.media.opengl.GLEventListener;
import javax.media.opengl.GLProfile;
import javax.media.opengl.awt.GLCanvas;

import com.jogamp.opengl.util.FPSAnimator;
import com.jogamp.opengl.util.glsl.ShaderCode;
import com.jogamp.opengl.util.glsl.ShaderProgram;


public class DemoGLES2  implements GLEventListener {

    private static final long serialVersionUID = 1L;

//    private final Animator animator;
    private int program;
    private int[] vertexArray = null;

    
    
 public static void main(String[] args) {
    	
	 DemoGLES2 me = new DemoGLES2();
    	
        GLProfile glprofile = GLProfile.getDefault();
        GLCapabilities glcapabilities = new GLCapabilities( glprofile );
        GLCanvas canvas = new GLCanvas( glcapabilities );
        
        Frame frame = new Frame("AWT Window Test");
        frame.setSize(800, 600);
        frame.add(canvas);
        frame.setResizable(false);
        frame.setVisible(true);
        
        
        // by default, an AWT Frame doesn't do anything when you click
        // the close button; this bit of code will terminate the program when
        // the window is asked to close
        frame.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        });
        
        //Mouse Handler
        
        canvas.addGLEventListener(me);
        
        // Esse é o inferno que fica fazendo chamar o display por fps - precisa!
        FPSAnimator animator = new FPSAnimator(canvas, 60);
        animator.start();
        
    }
 
    
    public void Demo3() {
        /*super("Demo3: Ch2 Pg18");
        super.initOpenGL();
        setVisible(true);
        vertexArray = new int[1];
        animator = new Animator(canvas);
        animator.start();*/
    }

    @Override
    public void init(GLAutoDrawable drawable) {
    	GLES3 gl = drawable.getGL().getGLES3();
        drawable.setGL(new DebugGLES3(gl));
        program = createProgram(gl);
//        gl.glGenVertexArrays(vertexArray.length, vertexArray, 0);
  //      gl.glBindVertexArray(vertexArray[0]);
    }

    @Override
    public void display(GLAutoDrawable drawable) {
    	GLES3 gl = drawable.getGL().getGLES3();
        FloatBuffer color = FloatBuffer.allocate(4);
        color.put(0, (float) (Math.sin(System.currentTimeMillis()/100.0)*0.5 + 0.5));
        color.put(1, (float) (Math.cos(System.currentTimeMillis()/100.0)*0.5 + 0.5));
        color.put(2, 0.0f);
        color.put(3, 1.0f);
//        gl.glClearBufferfv(GLES2.GL_COLORGL_COLOR, 0, color);
        gl.glUseProgram(program);
//        gl.glPointSize(60.0f);
        gl.glDrawArrays(GLES3.GL_POINTS, 0, 1);
    }

    private int createProgram(GLES3 gl) {
        String vertexShaderSource =
                "#version 440 core                              \n" +
                "                                               \n" +
                "void main(void)                                \n" +
                "{                                              \n" +
                "    gl_Position = vec4(0.0f, 0.0f, 0.5f, 1.0f);\n" +
                "}                                              \n";
        String fragmentShaderSource =
                "#version 440 core                              \n" +
                "                                               \n" +
                "out vec4 color;                                \n" +
                "                                               \n" +
                "void main(void)                                \n" +
                "{                                              \n" +
                "    color = vec4(1.0f, 1.0f, 1.0f, 1.0f);      \n" +
                "}                                              \n";
        String[][] sources = new String[1][1];

        sources[0] = new String[]{ vertexShaderSource };
        ShaderCode vertexShaderCode = new ShaderCode(GLES3.GL_VERTEX_SHADER, sources.length, sources);
        boolean compiled = vertexShaderCode.compile( gl, System.err);
        if (!compiled)
            System.err.println("[error] Unable to compile vertex shader: " + sources);

        sources[0] = new String[]{ fragmentShaderSource };
        ShaderCode fragmentShaderCode = new ShaderCode(GLES3.GL_FRAGMENT_SHADER, sources.length, sources);
        compiled = fragmentShaderCode.compile( gl, System.err);
        if (!compiled)
            System.err.println("[error] Unable to compile fragment shader: " + sources);

        ShaderProgram program = new ShaderProgram();
        program.init(gl);
        program.add(vertexShaderCode);
        program.add(fragmentShaderCode);
        program.link(gl, System.out);

        if(!program.validateProgram(gl, System.out))
            System.err.println("[error] Unable to link program");

        return program.program();
    }

    @Override
    public void dispose(GLAutoDrawable drawable) {
    	GLES3 gl = drawable.getGL().getGLES3();;
        //animator.stop();
      //  gl.glDeleteVertexArrays(vertexArray.length, vertexArray, 0);
        gl.glDeleteProgram(program);
    }

    @Override
    public void reshape(GLAutoDrawable drawable, int x, int y, int width,
            int height) {
    	GLES3 gl =drawable.getGL().getGLES3();;
        gl.glViewport(x, y, width, height);
    }

}