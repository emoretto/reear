package br.eng.moretto.earmaker.gl3d;

import java.awt.image.BufferedImage;
import java.nio.FloatBuffer;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;

import javax.vecmath.Vector3f;

import wblut.geom.WB_Point;
import wblut.geom.WB_Triangle;
import wblut.hemesh.HEC_FromTriangles;
import wblut.hemesh.HET_Export;
import wblut.hemesh.HE_Mesh;

import com.jogamp.common.nio.Buffers;

public class Object3D {
	
	int[] N;
	int w = 1;
	int h = 1;
	int l = 0;
	int currentL=0;
	
	public double scaleX;
	public double scaleY;
	public double scaleZ;
	
	float boundaryBoxMaxX=0;
	
	LinkedList<Cubo> cubos;
	
	//compensacao de valor de pixel na interpolacao do MC
	//se deixar ele 0d, nao ira compensar e o mesh ficara com as dimensoes exatas em suas medidas
	//se deixar ele com 127.5 ficara com o modelo exato porem com as paredes inferiores
	static double isoValue = 0d; 

	byte[][][] fatias;
	
	
	HE_Mesh mesh = new HE_Mesh();
	int size = 0; 
	
	FloatBuffer triangle;
    FloatBuffer normal;
    
	private boolean smooth = true;
	
	public Object3D(int w, int h, int l) {
		this.w = w;
		this.h = h;
		this.l = l;		
		fatias = new byte[w][h][l];			
		N = new int[]{w,h,l};
		System.out.println("New Object3D "+w+","+h+","+l);
	}
	
	public void setImage(BufferedImage buff, int slice){
		System.out.println("adding image to layer "+slice);
		for(int x =0; x<w; x++){
			for(int y=0; y<h; y++){
				
				if( (buff.getRGB(x, y) & 0xff) > 0)
					fatias[x][y][slice] = Byte.MIN_VALUE;
				else
					fatias[x][y][slice] = Byte.MAX_VALUE;
			}
		}
	}

	public void addImage(BufferedImage buff){
		
		for(int x =0; x<w; x++){
			for(int y=0; y<h; y++){
				if( (buff.getRGB(x, y) & 0xff) > 0)
					fatias[x][y][currentL] = Byte.MIN_VALUE;
				else
					fatias[x][y][currentL] = Byte.MAX_VALUE;
			}
		}
		currentL++;
	}
	
	/**
	 * Marching Cubes
	 */
	public void mc(){

		System.out.println("MC Start");
		// Todas as imagens serao varridas
		cubos = new LinkedList<Cubo>();
		for(int i =0; i< w; i++){
			for(int j=0;j<h;j++){
				for(int k =0; k< l; k++){
					
					// O cubo deslizante é criado
					Cubo cubo = new Cubo();
					VerticeMC[] vertices = new VerticeMC[8];
					vertices[0] = new VerticeMC(i,j,k);					
					vertices[1] = new VerticeMC(i,j+1,k);
					vertices[2] = new VerticeMC(i+1,j+1,k);
					vertices[3] = new VerticeMC(i+1,j,k);
					vertices[4] = new VerticeMC(i,j,k+1);
					vertices[5] = new VerticeMC(i,j+1,k+1);
					vertices[6] = new VerticeMC(i+1,j+1,k+1);
					vertices[7] = new VerticeMC(i+1,j,k+1);
					
					// e cada um dos 8 cantos do cubo terá um peso (valor)
					// esse valor é calculado em função do valor do pixel
					// encontrado na imagem. (-127 para quando nao tiver pixel e 127 qdo tiver pixel)
					for(int w=0; w<8; w++){
						vertices[w].setValor(valorPeso(vertices[w]));
					}
					cubo.setVertices(vertices);
					
					// Aqui efetivamente é feito o MC
					// para cada aresta é feita a interpolação de onde o pixel da imagem cruza o cubo
					// É escolhido o melhor cubo possível dentre os 15 possíveis
					cubo.poligonizar(new Double(isoValue));

					// se esse cubo tiver triangulos
					if(cubo.getTriangulos() != null){
						cubos.add(cubo);
					}
				}
			}
		}
		//fatias = null;
		System.out.println("End of MC. Cubes: "+cubos.size());
	}
	
	
	/**
	 * Aqui passamos os Cubos (contendo os triangulos) obtidos pelo MC para a estrutura de dados da biblioteca HE_Mesh
	 * Essa biblioteca facilita a aplicacao de filtros de smooth e exporta direto para STL
	 * 
	 */
	public void hemesh(){
		
		ArrayList<WB_Triangle> wbTriangles = new ArrayList<WB_Triangle>();
		
		// MC Cubes --> Triangles
		Iterator<Cubo> it = cubos.iterator();
		while(it.hasNext()){
			Cubo cubo = it.next();
			if(cubo.getTriangulos()!=null){
				//triangulos.addAll(cubo.getTriangulos());
				
				for(Triangle t : cubo.getTriangulos()){
					//HE_Face face = new HE_Face();
					/*mesh.add(new HE_Vertex(t.p[0].x,t.p[0].y,t.p[0].z));
					mesh.add(new HE_Vertex(t.p[1].x,t.p[1].y,t.p[1].z));
					mesh.add(new HE_Vertex(t.p[2].x,t.p[2].y,t.p[2].z));*/
										
					WB_Point p1 = new WB_Point(t.p[0].x,t.p[0].y,t.p[0].z);
					WB_Point p2 = new WB_Point(t.p[1].x,t.p[1].y,t.p[1].z);
					WB_Point p3 = new WB_Point(t.p[2].x,t.p[2].y,t.p[2].z);
				    
					WB_Triangle tri = new WB_Triangle(p1,p2,p3);
					wbTriangles.add(tri);
				}
			}
		}
		System.out.println("Triangles count: "+wbTriangles.size());
		
		// Uma vez com os triangulos todos na lista, vamos criar o MESH
		HEC_FromTriangles fromPolygons = new HEC_FromTriangles();
		fromPolygons.setTriangles(wbTriangles);
		
		mesh = fromPolygons.create();
		if(mesh.validate()) 
			System.out.println("Mesh OK!");
		
		//Applying scale
		if(scaleX > 0d)
			mesh.scale(scaleX, scaleY, scaleZ);
		
		size = wbTriangles.size() * 9;
		wbTriangles.clear();
		
		/* Smooth */
		if(smooth){
			SDLSmooth modifier = new SDLSmooth();
			modifier.setStepSize(0.18756f);
			//modifier.setIterations(12);
			modifier.setIterations(2);
			modifier.setKeepBoundary(true);
			modifier.setAutoRescale(true);// rescale mesh to original extents
			mesh.modify(modifier);
		}
		System.out.println("Uouu! It was beautiful!");
		

	}
	
	/**
	 * From HE_Mesh to GL Triangles
	 * Ready to Display
	 */
	public void createTriangles(){
		
		triangle = Buffers.newDirectFloatBuffer(size*9);
		normal = Buffers.newDirectFloatBuffer(size*9);
		
		for(WB_Triangle t : mesh.getTriangles()){
			
			triangle.put(t.p1().xf());
			triangle.put(t.p1().yf());
			triangle.put(t.p1().zf());
			
			triangle.put(t.p2().xf());
			triangle.put(t.p2().yf());
			triangle.put(t.p2().zf());
			
			triangle.put(t.p3().xf());
			triangle.put(t.p3().yf());
			triangle.put(t.p3().zf());

			//Normal recalculation - cuz smooth destroy normals
			Vector3f vec0 = new Vector3f(t.p1().xf(), t.p1().yf(), t.p1().zf());
			Vector3f vec1 = new Vector3f(t.p2().xf(), t.p2().yf(), t.p2().zf());
			Vector3f vec2 = new Vector3f(t.p3().xf(), t.p3().yf(), t.p3().zf());
	    	
			vec1.sub(vec0);    	
	    	vec2.sub(vec0);
	    	Vector3f vecNorm = new Vector3f(0.0f, 0.0f, 0.0f);
	    	vecNorm.cross(vec1, vec2);
	    	vecNorm.normalize();
			
			normal.put(vecNorm.x);
			normal.put(vecNorm.y);
			normal.put(vecNorm.z);
			
			normal.put(vecNorm.x);
			normal.put(vecNorm.y);
			normal.put(vecNorm.z);
			
			normal.put(vecNorm.x);
			normal.put(vecNorm.y);
			normal.put(vecNorm.z);
		}
	}
	
	
	public double valorPeso(VerticeMC v){
    	
		int ix = (int)((v.x/w)*N[0]);
		int iy = (int)((v.y/h)*N[1]);
		int iz = (int)((v.z/l)*N[2]);
		double fx = (v.x/w)*N[0] - ix;
		double fy = (v.y/h)*N[1] - iy;
		double fz = (v.z/l)*N[2] - iz;
		double A = 0;
		double B = 0;
		double C = 0;
		double D = 0;
		double E = 0;
		double F = 0;
		double G = 0;
		
		if(ix<N[0] && iy<N[1] && iz<N[2]){
			if(ix+1<N[0]){
				A = (1-fx)*fatias[ix][iy][iz] + fx*fatias[ix+1][iy][iz];		
				if(iy+1<N[1]){
					B= (1-fx)*fatias[ix][iy+1][iz] + fx*fatias[ix+1][iy+1][iz];
					if(iz+1<N[2]){
						D= (1-fx)*fatias[ix][iy+1][iz+1] + fx*fatias[ix+1][iy+1][iz+1]; 
					}
				}
				if(iz+1<N[2]){
					C= (1-fx)*fatias[ix][iy][iz+1] + fx*fatias[ix+1][iy][iz+1]; 
				}
			}
		}
		E=(1-fy)*A+fy*B;
		F=(1-fy)*C+fy*D;
		G=(1-fz)*E+fz*F;
		return G;
	}

	/**
	 * Make All
	 * From slices to 3d mesh
	 */
	public void make() {
		this.mc();
		this.hemesh();
		this.createTriangles();
	}
	
	public void saveSTL(String path, String name){
		HET_Export.saveToSTL(mesh, path, name);
	}
}
