package br.eng.moretto.earmaker.gl3d;
 
import ij.IJ;
import ij.ImagePlus;

import java.awt.Frame;
import java.awt.Point;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.nio.IntBuffer;
import java.util.TreeMap;

import javax.imageio.ImageIO;
import javax.media.opengl.GL;
import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLCapabilities;
import javax.media.opengl.GLEventListener;
import javax.media.opengl.GLProfile;
import javax.media.opengl.awt.GLCanvas;
import javax.media.opengl.glu.GLU;
import javax.media.opengl.glu.GLUquadric;

import com.jogamp.opengl.util.FPSAnimator;
import com.jogamp.opengl.util.gl2.GLUT;

/**
 * USAR AWT 
 * DESABILITAR O -XstartOnFirstThread (checkbox default no eclipse)
 * USAR ESSES VM PARAMS: -Xmx8G -Djava.ext.dirs=''
 * 
 * @author emoretto
 *
 */
public class Viewer3D implements GLEventListener{
	
	public boolean drawWires = false;
	public boolean drawFog = false;
	public boolean needsUpdate = false;
	public float zoom = -150.0f;
	
	private static final int _WINDOW_WIDTH = 800;
	private static final int _WINDOW_HEIGHT = 640;

	private Object3D object3d;
	
	// GL stuffs
    private GLU glu = new GLU();
    private GLUT glut = new GLUT();
    private GLUquadric quadratic;        
    private FPSAnimator animator;
    private IntBuffer buffers = IntBuffer.allocate(2);
    
    private Matrix4f lastRot = new Matrix4f();
    private Matrix4f thisRot = new Matrix4f();
    private final Object matrixLock = new Object();
    private float[] matrix = new float[16];

    private ArcBall arcBall = new ArcBall(_WINDOW_WIDTH, _WINDOW_HEIGHT);  
    
    //OpenGL
    float[] colorAmbient = {0.9f, 0.9f, 0.9f, 1.0f};
    float[] colorBlack  = {0.0f,0.0f,0.0f,1.0f};
    float[] colorWhite  = {1.0f,1.0f,1.0f,1.0f};
    float[] colorGray   = {0.9f,0.9f,0.9f,1.0f};
    float[] colorRed    = {1.0f,0.0f,0.0f,1.0f};
    float[] colorBlue   = {0.0f,0.0f,0.1f,1.0f};
    float[] colorYellow = {1.0f,1.0f,0.0f,1.0f};
    float[] colorLightYellow = {.5f,.5f,0.0f,1.0f};
	
	
    public static void main(String[] args) {
    	Viewer3D me = new Viewer3D("/Users/emoretto/workspace.mestrado/earmaker/resources/MC/");
    	me.setup();
    }
    
    public Viewer3D(String path) {
    	if(path != null && !path.isEmpty())
    		loadFromFiles(path);
    }
    
    public Viewer3D(Object3D object3d){
    	this.object3d = object3d;
    }
    
    public void reload(){
    	// Make all - MC, HEMesh and Triangles to GL
    	object3d.make();
    	needsUpdate = true;
    }
    
    

    public void setup(){
    	
    	// Make all - MC, HEMesh and Triangles to GL
    	object3d.make();
    	
        GLProfile glprofile = GLProfile.getDefault();
        GLCapabilities glcapabilities = new GLCapabilities( glprofile );
        GLCanvas canvas = new GLCanvas( glcapabilities );
        
        Frame frame = new Frame("Visualizador 3D");
        frame.setSize(_WINDOW_WIDTH, _WINDOW_HEIGHT);
        frame.add(canvas);
        frame.setResizable(false);
        frame.setVisible(true);
        
        
        // by default, an AWT Frame doesn't do anything when you click
        // the close button; this bit of code will terminate the program when
        // the window is asked to close
        frame.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        });
        
        //Mouse Handler
        InputHandler inputHandler = new InputHandler(this);        
        
        canvas.addGLEventListener(this);
        canvas.addMouseListener(inputHandler);
        canvas.addMouseMotionListener(inputHandler);
        canvas.addMouseWheelListener(inputHandler);
        
        canvas.setAutoSwapBufferMode(true);
        
        // Esse é o inferno que fica fazendo chamar o display por fps - precisa!
        animator = new FPSAnimator(canvas, 60);
        animator.setUpdateFPSFrames(3, null);
        animator.start();
    }
    
    public void loadFromFiles(String path){

    	//IO
    	try {
			TreeMap<String,BufferedImage> images = new TreeMap<String,BufferedImage>();
			File dir = new File(path);
			
			for(File file : dir.listFiles()){
				
				if(file.getName().endsWith("png")){
					System.out.println("Loading..." +file.getName());
					images.put(file.getName(), ImageIO.read(file));
				}
			}			
			
			int w = images.get(images.firstKey()).getWidth();
			int h = images.get(images.firstKey()).getHeight();
			int l = images.size();		
			
			object3d = new Object3D(w, h, l);
			
	    	for(String key : images.keySet()){
	    		ImagePlus ip = new ImagePlus("fuck", images.get(key));
	    		IJ.run(ip,"Make Binary", "calculate");
				object3d.addImage( ip.getBufferedImage() );
			}
	    	
			images.clear();
			System.out.println("End of To Matrix");
	    	
    	} catch (Exception e1) {
			e1.printStackTrace();
		}
    }
    

    public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height) {
    	GL2 gl = drawable.getGL().getGL2();

        gl.glViewport(0, 0, width, height);                // Reset The Current Viewport
        gl.glMatrixMode(GL2.GL_PROJECTION);                                        // Select The Projection Matrix
        gl.glLoadIdentity();                                                    // Reset The Projection Matrix
        //gl.glOrthof(-10f, 10f, -10f, 10f, 0f, 50f);
        glu.gluPerspective(45.0f, (float) (width) / (float) (height), 1.0f, 100.0f);           // Calculate The Aspect Ratio Of The Window
                
        gl.glMatrixMode(GL2.GL_MODELVIEW);                                        // Select The Modelview Matrix
        gl.glLoadIdentity();                                                    // Reset The Modelview Matrix

        arcBall.setBounds((float) width, (float) height);                 //*NEW* Update mouse bounds for arcball
    }
    
    public void displayChanged(GLAutoDrawable drawable, boolean modeChanged, boolean deviceChanged) {
        init(drawable);
    }
 
    public void doFog( GL2 gl )
    {
        int FOG_DISTANCE = 90;
        float[] fogcol =    { 0, 0, 0, 0 };
        gl.glEnable(GL2.GL_FOG);
        gl.glFogi(GL2.GL_FOG_MODE, GL.GL_LINEAR);
        gl.glFogf(GL2.GL_FOG_DENSITY, 0f);
        gl.glFogfv(GL2.GL_FOG_COLOR, fogcol, 0);
        gl.glFogf(GL2.GL_FOG_START, FOG_DISTANCE - 30);
        gl.glFogf(GL2.GL_FOG_END, FOG_DISTANCE);
        gl.glHint(GL2.GL_FOG_HINT, GL.GL_NICEST);
    }
    
    
    public void init(GLAutoDrawable drawable) {
    	
    	GL2 gl = drawable.getGL().getGL2();

        // Start Of User Initialization
        lastRot.setIdentity();                                // Reset Rotation
        thisRot.setIdentity();                                // Reset Rotation
        thisRot.get(matrix);

        gl.glClearColor(0.0f, 0.0f, 0.0f, 0.5f);                            // Black Background
        gl.glClearDepth(1.0f);                                            // Depth Buffer Setup
        gl.glDepthFunc(GL.GL_LEQUAL);                                        // The Type Of Depth Testing (Less Or Equal)
        gl.glEnable(GL.GL_DEPTH_TEST);                                        // Enable Depth Testing
        //gl.glShadeModel(GL2.GL_FLAT);                                            // Select Flat Shading (Nice Definition Of Objects)
        gl.glHint(GL2.GL_PERSPECTIVE_CORRECTION_HINT, GL.GL_NICEST);                // Set Perspective Calculations To Most Accurate

        quadratic = glu.gluNewQuadric();                                        // Create A Pointer To The Quadric Object
        glu.gluQuadricNormals(quadratic, GLU.GLU_SMOOTH);                        // Create Smooth Normals
        glu.gluQuadricTexture(quadratic, true);                            // Create Texture Coords

        /** Lighting */
    	//gl.glEnable(GL2.GL_LIGHT1); // Enable Default Light
        gl.glEnable(GL2.GL_LIGHTING);                                            // Enable Lighting
        gl.glEnable(GL2.GL_LIGHT0);  

        gl.glEnable(GL2.GL_CULL_FACE);
        gl.glCullFace(GL.GL_BACK);
        
        float posLight1[] = { 0.0f, 3.0f, 10.0f, 1.0f };
        float spotDirection[] = { 0.0f, 0.0f, -1.0f };
        
        gl.glLightfv( GL2.GL_LIGHT0, GL2.GL_POSITION, posLight1 ,0);
        gl.glLightfv( GL2.GL_LIGHT0, GL2.GL_SPOT_DIRECTION, spotDirection, 0);
        gl.glLightfv( GL2.GL_LIGHT0, GL2.GL_AMBIENT, colorAmbient, 0);
        gl.glLightfv( GL2.GL_LIGHT0, GL2.GL_DIFFUSE, colorGray, 0 );
        
        //Qde de buffers
        gl.glGenBuffers(2, buffers);
		
		// Enable same as for vertex buffers.
		gl.glEnableClientState( GL2.GL_VERTEX_ARRAY );
		gl.glEnableClientState( GL2.GL_NORMAL_ARRAY );
		 
		// Copiando pra placa de video
		copyMeshToBuffers(gl);
    }

	private void copyMeshToBuffers(GL2 gl) {
		
		// Init VBOs and transfer data.
		gl.glBindBuffer( GL2.GL_ARRAY_BUFFER, buffers.get(0) );
		 
		/** Data */
		// Copy data to the server into the VBO.
		object3d.triangle.rewind();
		object3d.normal.rewind();
		gl.glBufferData( GL2.GL_ARRAY_BUFFER, object3d.size*9, object3d.triangle, GL2.GL_STATIC_DRAW );
		gl.glVertexPointer( 3, GL.GL_FLOAT, 0, 0 );
		 

		/** Normals */
		// Init VBOs and transfer data.
		gl.glBindBuffer( GL2.GL_ARRAY_BUFFER, buffers.get(1) );
		 
		// Copy data to the server into the VBO.
		gl.glBufferData( GL2.GL_ARRAY_BUFFER, object3d.size*9, object3d.normal, GL2.GL_STATIC_DRAW );
		gl.glNormalPointer( GL.GL_FLOAT, 0, 0 );
	}
    
    void reset() {
        synchronized(matrixLock) {
            lastRot.setIdentity();   // Reset Rotation
            thisRot.setIdentity();   // Reset Rotation
        }
    }

    void startDrag( Point MousePt ) {
        synchronized(matrixLock) {
            lastRot.set( thisRot );                                        // Set Last Static Rotation To Last Dynamic One
        }
        arcBall.click( MousePt );                                 // Update Start Vector And Prepare For Dragging
    }
 
    void drag( Point MousePt )       // Perform Motion Updates Here
    {
        Quat4f ThisQuat = new Quat4f();
 
        // Update End Vector And Get Rotation As Quaternion
        arcBall.drag( MousePt, ThisQuat); 
        synchronized(matrixLock) {
            thisRot.setRotation(ThisQuat);  // Convert Quaternion Into Matrix3fT
            thisRot.mul( thisRot, lastRot); // Accumulate Last Rotation Into This One
        }
    }
 
    void zoom(int value){
   		zoom += ((float) value)/8f;
    }
    
    void renderStrokeString(GL2 gl, int font, String string) {
        // Render The Text
        for (int i = 0; i < string.length(); i++) {
            char c = string.charAt(i);
            glut.glutStrokeCharacter(font, c);
        }
    }
    
    
    @Override
    public void display(GLAutoDrawable drawable) {
    	
    	synchronized(matrixLock) {
            thisRot.get(matrix);
        }
    	
        GL2 gl = drawable.getGL().getGL2();
 
        gl.glMatrixMode(GL2.GL_PROJECTION);                                        // Select The Projection Matrix
        gl.glLoadIdentity();                                                    // Reset The Projection Matrix
        //gl.glOrthof(-10f, 10f, -10f, 10f, 0f, 50f);
        glu.gluPerspective(45.0f, (float) (_WINDOW_WIDTH) / (float) (_WINDOW_HEIGHT), 0.1f, 1000.0f);           // Calculate The Aspect Ratio Of The Window
        gl.glMatrixMode(GL2.GL_MODELVIEW);     
        
        // Clear Screen And Depth Buffer
        gl.glClear(GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT);                
         
        gl.glLoadIdentity();   
        
        gl.glPolygonMode( GL2.GL_FRONT, GL2.GL_FILL );
        //gl.glPolygonMode( GL2.GL_FRONT_AND_BACK, GL2.GL_LINE );
        
        if(drawFog )
        	doFog(gl);
        
        
        
        
        // Zoom inicial
        gl.glTranslatef(0.0f, 0.0f, zoom);  
        gl.glPushMatrix();                  

        //Aqui esta sem a rotacao - bom pra fazer um fundo em perspectiva
        gl.glMultMatrixf(matrix, 0);        // NEW: Apply Dynamic Transform
        
        //Esfera DEBUG
        //glu.gluSphere(quadratic, 0.3f, 20, 20);
        
        gl.glDisable(GL2.GL_COLOR_MATERIAL);  
        //gl.glEnable(GL2.GL_LigOLOR_MATERIAL);  

        float[] rgba = {0.9f, 0.6f, 0.4f, 0.2f};
    	gl.glMaterialfv(GL.GL_FRONT, GL2.GL_DIFFUSE, rgba, 0);
    	

        
        //Centralizando o modelo em função do seu centro
    	gl.glTranslatef(object3d.mesh.getCenter().xf()*-1,
    			object3d.mesh.getCenter().yf()*-1,
    			object3d.mesh.getCenter().zf()*-1);
        
        
        // Copiando o buffer pra placa de video, quando necessario
        if(needsUpdate){
        	needsUpdate = false;
    		// Copiando pra placa de video
    		copyMeshToBuffers(gl);
        }	 
 		
        
        // Draw
        gl.glDrawArrays( GL.GL_TRIANGLES, 0, object3d.size*3 );
        gl.glEnable( GL.GL_POLYGON_OFFSET_FILL );      
        gl.glPolygonOffset( 1f, 1f );
        
        // Wires
        if(drawWires ){
	        float[] rgbaWire = {0.8f, 0.5f, 0.3f, 0.2f};
			
	        gl.glPolygonMode( GL2.GL_FRONT, GL2.GL_LINE );        
	        gl.glMaterialfv(GL.GL_FRONT, GL2.GL_DIFFUSE, rgbaWire, 0);
	        
	        gl.glEnable( GL2.GL_POLYGON_OFFSET_LINE );
	        gl.glPolygonOffset( 2.0f, 2.0f );
	        
	        gl.glDrawArrays( GL.GL_TRIANGLES, 0, object3d.size*3 );
	        
	        gl.glDisable( GL2.GL_POLYGON_OFFSET_LINE );   
        }
        
        
		
        //Zerando a projecao e mudando pra modelview (2D)
        gl.glMatrixMode(GL2.GL_PROJECTION);    
        gl.glLoadIdentity();

        gl.glMatrixMode(GL2.GL_MODELVIEW);    
        gl.glLoadIdentity();

        //FPS text
        gl.glTranslatef(-0.99f, 0.95f, 0f);
        gl.glScalef(0.0002f, 0.0002f, 1f);
        gl.glColor3f(1.0f, 0.0f, 0.0f);
        renderStrokeString(gl, GLUT.STROKE_MONO_ROMAN, "FPS "+ Math.round(animator.getLastFPS()));
        
        
        gl.glPopMatrix();                   // NEW: Unapply Dynamic Transform
        gl.glFlush();                       // Flush The GL Rendering Pipeline
    }

	@Override
	public void dispose(GLAutoDrawable drawable) {
	}
}