package br.eng.moretto.earmaker.gl3d;

import javax.vecmath.Point3d;

public class VerticeMC extends Point3d{
	private static final long serialVersionUID = 1L;
	
	private Double valor;

	public VerticeMC(double x, double y, double z){
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	public VerticeMC(){
	}
	
	public VerticeMC(Double valor) {
		super();
		this.valor = valor;
	}

	public Double getValor() {
		return valor;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}
	
	public void calcPeso(int w, int h, int l, int[] N, byte[][][] fatias){
    	
		int ix = (int)((this.x/w)*N[0]);
		int iy = (int)((this.y/h)*N[1]);
		int iz = (int)((this.z/l)*N[2]);
		double fx = (this.x/w)*N[0] - ix;
		double fy = (this.y/h)*N[1] - iy;
		double fz = (this.z/l)*N[2] - iz;
		double A = 0;
		double B = 0;
		double C = 0;
		double D = 0;
		double E = 0;
		double F = 0;
		double G = 0;
		
		
		if(ix<N[0] && iy<N[1] && iz<N[2]){
			if(ix+1<N[0]){
				A = (1-fx)*fatias[ix][iy][iz] + fx*fatias[ix+1][iy][iz];		
				if(iy+1<N[1]){
					B= (1-fx)*fatias[ix][iy+1][iz] + fx*fatias[ix+1][iy+1][iz];
					if(iz+1<N[2]){
						D= (1-fx)*fatias[ix][iy+1][iz+1] + fx*fatias[ix+1][iy+1][iz+1]; 
					}
				}
				if(iz+1<N[2]){
					C= (1-fx)*fatias[ix][iy][iz+1] + fx*fatias[ix+1][iy][iz+1]; 
				}
			}
		}
		E=(1-fy)*A+fy*B;
		F=(1-fy)*C+fy*D;
		G=(1-fz)*E+fz*F;
		
		this.valor = G;
	}
	
	
}
