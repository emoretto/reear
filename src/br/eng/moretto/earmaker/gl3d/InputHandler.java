package br.eng.moretto.earmaker.gl3d;
 
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;

import javax.swing.SwingUtilities;
import javax.swing.event.MouseInputAdapter;
 
class InputHandler extends MouseInputAdapter {
	
    private Viewer3D renderer;
 
    public InputHandler(Viewer3D renderer) {
        this.renderer = renderer;
    }
 
    @Override
    public void mouseWheelMoved(MouseWheelEvent e) {
    	renderer.zoom(e.getWheelRotation());
    }
    
    public void mouseClicked(MouseEvent e) {
        if (SwingUtilities.isRightMouseButton(e)) {
            renderer.reset();
        }
    }
 
    public void mousePressed(MouseEvent mouseEvent) {
        if (SwingUtilities.isLeftMouseButton(mouseEvent)) {
            renderer.startDrag(mouseEvent.getPoint());
        }
    }
 
    public void mouseDragged(MouseEvent mouseEvent) {
        if (SwingUtilities.isLeftMouseButton(mouseEvent)) {
            renderer.drag(mouseEvent.getPoint());
        }
    }
}
 
 
