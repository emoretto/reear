package br.eng.moretto.earmaker.gl3d;

import javax.vecmath.Point3f;
import javax.vecmath.Vector3f;

public class Triangle {

	public Point3f p[] = new Point3f[3];
	Vector3f vecNorm = new Vector3f(0.0f, 0.0f, 0.0f);
	
	public Triangle(Point3f[] p) {
		this.p = p;
		calcNormal();
	}
	
	
	private void calcNormal() {
		
		Vector3f vec0 = new Vector3f(p[0].x, p[0].y, p[0].z*2f);
    	Vector3f vec1 = new Vector3f(p[1].x, p[1].y, p[1].z*2f);
    	Vector3f vec2 = new Vector3f(p[2].x, p[2].y, p[2].z*2f);
    	
		vec1.sub(vec0);    	
    	vec2.sub(vec0);
    	vecNorm.cross(vec1, vec2);
    	vecNorm.normalize();
	}
	
	
}
