package br.eng.moretto.earmaker.gl3d;

import java.util.LinkedList;
import java.util.List;
import java.util.Vector;

import javax.vecmath.Point3f;
import javax.vecmath.Vector3f;

import wblut.geom.WB_Point;
import wblut.hemesh.HE_Halfedge;
import wblut.hemesh.HE_Mesh;
import wblut.hemesh.HE_Vertex;

/**
 * 
 * HE_MESH - biblioteca MESH..
 * Fazer na mão seria tenso.. seria uma espécie de hashtable usando as posiçoes x,y,z
 * 
 * 
 * Zica severa no halfedges.. mesmo com a lib nao ta retornando os halfedges..
 * 
 * https://github.com/wblut/HE_Mesh2014/blob/master/hemesh/examples/tutorial/Halfedge/Halfedge.pde
 * http://www.flipcode.com/archives/The_Half-Edge_Data_Structure.shtml
 * 
 * 
 * @author emoretto
 *
 */
public class SDSmooth {
	
	void scaleDependentSmoothing(HE_Mesh mesh, int numIteration, double step_size, boolean protectBorders)
	{
	        System.out.println("\nPerforming Scale Dependent Smoothing (iterations = "+numIteration+")...");
	
	        //Surface_mesh::Vertex_property<Point> points = m->vertex_property<Point>("v:point");
//	        Surface_mesh::Vertex_property<Point> newPositions = m->vertex_property<Point>("v:new_point");
//	        Surface_mesh::Vertex_iterator vit, vend = m->vertices_end();
	        
//	        List<Float> newPositions = new LinkedList<Float>();
	
	        for(int iteration = 0; iteration < numIteration; iteration++)
	        {
	                // Original positions, for boundary
	        		//for(HE_Vertex vertex : mesh.getVerticesAsList()){
	        			//newPositions = vertex.getPoint()
	        		//}
//	                for(vit = m->vertices_begin(); vit != vend; ++vit)
//	        		for(Float vit : points){
//                        newPositions[vit] = points[vit];
	
	                // Compute scale-dependent Laplacian
	                //for(vit = m->vertices_begin(); vit != vend; ++vit)
	        		for(HE_Vertex vertex : mesh.getVerticesAsList()){
	                        //if(!protectBorders || (protectBorders && !m->is_boundary(vit)))
	                		Vector3f newVertex = scaleDependentSmoothVertex(mesh, vertex, step_size);
	                		vertex.setX(newVertex.x);
	                		vertex.setY(newVertex.y);
	                		vertex.setZ(newVertex.z);
	                }
	
	                // Set vertices to final position
	                //for(HE_Vertex vertex : mesh.getVerticesAsList())
	               //         points[vit] = newPositions[vit];
	        }
	}
	
	Vector3f scaleDependentSmoothVertex(HE_Mesh mesh, HE_Vertex v, double step_size)
	{
	        //Surface_mesh::Vertex_property<Point> points = m->vertex_property<Point>("v:point");

			Vector3f pt = new Vector3f(v.xf(), v.yf(), v.zf());

	        Vector<Vector3f> nighbour = new Vector<Vector3f>();
	        Vector<Float> weight = new Vector<Float>();

	        Vector3f diff = new Vector3f();
	        Vector3f sigma = new Vector3f();
	        float E = 0.0f;

	        
	        mesh.getHalfedgeByKey(v.getKey());
	        
	        //Surface_mesh::Halfedge_around_vertex_circulator hit = m->halfedges(v),	hend = m->halfedges(v);
	        List<HE_Halfedge> hit = mesh.getVertexByKey(v.getKey()).getHalfedgeStar();
	   //     List<HE_Halfedge> hend = mesh.getVertexByKey(v.getKey()).getHalfedgeStar();
	        
	        for(HE_Halfedge half : hit){
	        
	    		Vector3f vj = new Vector3f(half.getVertex().xf(), half.getVertex().yf(), half.getVertex().zf());
		        
	    		vj.sub(pt);
        		vj.normalize();
        		float edgeLen = vj.length();
        	

                if(edgeLen == 0){
                    E = 0;  
                    break;
                }

                E += edgeLen;

                weight.add(edgeLen);
                nighbour.add( vj );
                
	        }
	        /*
	        do{

//        		  Point3f vj = m->to_vertex(hit);
//                double edgeLen = (points[vj] - pt).norm();
	        		if(hit == null)
	        			return pt;
	        		
	        		Vector3f vj = new Vector3f(hit.getVertex().xf(), hit.getVertex().yf(), hit.getVertex().zf());
	        		//WB_Point edge = hit.getVertex().getPoint().sub(v);
	        		
	        		vj.sub(pt);
	        		vj.normalize();
	        		float edgeLen = vj.length();
	        	

	                if(edgeLen == 0){
                        E = 0;  
                        break;
	                }

	                E += edgeLen;

	                weight.add(edgeLen);
	                nighbour.add( vj );
	                
	                hit = hit.getNextInFace();
	        } while( !hit.equals(hend) ); 
	        		//++hit != hend);
*/
	        // Normalize weights
	        if(E > 0)
	        {
	                for(int j = 0; j < (int) nighbour.size(); j++)
	                {
	                	
//	                	 	correct chance ratio = 1:10000000000000000000
//	                		weight[j] /= E;
//                        	sigma += (weight[j] * (nighbour[j] - pt));
	                	
	                        weight.set(j, weight.get(j) / E );
	                        
	                        Vector3f neigh = nighbour.get(j);
	                        neigh.sub(pt);
	                        
	                        neigh.scale(weight.get(j));
	                        sigma.add(neigh);
	                }
	                
	                //return pt + (sigma * step_size);
	    	        sigma.scale((float) step_size);
	    	        pt.add(sigma);
	    	        return pt;
	        }
	        else
	                return pt;
	}
}

