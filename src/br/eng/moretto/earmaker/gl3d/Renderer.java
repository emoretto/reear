package br.eng.moretto.earmaker.gl3d;
 
import java.awt.Frame;
import java.awt.Point;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.TreeMap;

import javax.imageio.ImageIO;
import javax.media.opengl.GL;
import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLCapabilities;
import javax.media.opengl.GLEventListener;
import javax.media.opengl.GLProfile;
import javax.media.opengl.awt.GLCanvas;
import javax.media.opengl.glu.GLU;
import javax.media.opengl.glu.GLUquadric;
import javax.vecmath.Vector3f;

import wblut.geom.WB_Point;
import wblut.geom.WB_Triangle;
import wblut.hemesh.HEC_FromTriangles;
import wblut.hemesh.HEM_Smooth;
import wblut.hemesh.HE_Mesh;

import com.jogamp.common.nio.Buffers;
import com.jogamp.opengl.util.FPSAnimator;
import com.jogamp.opengl.util.gl2.GLUT;

/**
 * USAR AWT 
 * DESABILITAR O -XstartOnFirstThread (checkbox default no eclipse)
 * USAR ESSES VM PARAMS: -Xmx8G -Djava.ext.dirs=''
 * 
 * @author emoretto
 *
 */
class Renderer implements GLEventListener{
	
	static float zSpace = 2f;
	
	 private static final int _WINDOW_HEIGHT = 640;
	private static final int _WINDOW_WIDTH = 800;
	// User Defined Variables
	private GLUquadric quadratic;                                            // Used For Our Quadric
    private GLU glu = new GLU();
    GLUT glut = new GLUT();
    static FPSAnimator animator;

    IntBuffer vertexArray = IntBuffer.allocate(1);
    private static FloatBuffer triangle;
    private static FloatBuffer normal;
    
    static int size = 0; //TODO triangulos.size() * 9
 //    static float[] triangleArray = new float[size * 9];
//	static float[] normalArray = new float[size * 9];
    
    private IntBuffer buffers = IntBuffer.allocate(2);
    

    static HE_Mesh mesh = new HE_Mesh();
    
    private Matrix4f LastRot = new Matrix4f();
    private Matrix4f ThisRot = new Matrix4f();
    private final Object matrixLock = new Object();
    private float[] matrix = new float[16];
    
    

    private ArcBall arcBall = new ArcBall(_WINDOW_WIDTH, _WINDOW_HEIGHT);  
    
    float zoom = 0.0f;
//    float[] rgba = {0.9f, 0.6f, 0.4f, 0.2f};
    //OpenGL
    float[] colorAmbient = {0.9f, 0.9f, 0.9f, 1.0f};
    float[] colorBlack  = {0.0f,0.0f,0.0f,1.0f};
    float[] colorWhite  = {1.0f,1.0f,1.0f,1.0f};
    float[] colorGray   = {0.9f,0.9f,0.9f,1.0f};
    float[] colorRed    = {1.0f,0.0f,0.0f,1.0f};
    float[] colorBlue   = {0.0f,0.0f,0.1f,1.0f};
    float[] colorYellow = {1.0f,1.0f,0.0f,1.0f};
    float[] colorLightYellow = {.5f,.5f,0.0f,1.0f};
    
    
    //MC
    static LinkedList<Cubo> cubos;
    //static LinkedList<Triangle> triangulos = new LinkedList<Triangle>();
    static byte[][][] fatias;
    static int[] N;
	static double isoValue = 100;
	static int w = 1;
	static int h = 1;
	static int l = 1;
	
    public static void main(String[] args) {
    	
    	//IO
    	try {
			TreeMap<String,BufferedImage> images = new TreeMap<String,BufferedImage>();
			File dir = new File("/Users/emoretto/workspace.mestrado/earmaker/resources/kss_out/");
			
			for(File file : dir.listFiles()){
				
				if(file.getName().endsWith("png")){
					System.out.println("Loading..." +file.getName());
					images.put(file.getName(), ImageIO.read(file));
				}
			}
			
			w = images.get(images.firstKey()).getWidth();
			h = images.get(images.firstKey()).getHeight();
			l = images.size();			
			fatias = new byte[w][h][l];			
			N = new int[]{w,h,l};
			
			System.out.println("To Matrix");
			int k=0;
			for(String key : images.keySet()){
				
				BufferedImage buff = images.get(key);
				System.out.println(key);
				for(int x =0; x<w; x++){
					for(int y=0; y<h; y++){
						
						if( (buff.getRGB(x, y) & 0xff) > 0)
							fatias[x][y][k] = Byte.MIN_VALUE;
						else
							fatias[x][y][k] = Byte.MAX_VALUE;
					}
				}
				k++;
			}
			images.clear();
			System.out.println("End of To Matrix");
			
			
			System.out.println("MC Start");
			cubos = new LinkedList<Cubo>();
			for(int i =0; i< w; i++){
				for(int j=0;j<h;j++){
					for(k =0; k< l; k++){
						
						Cubo cubo = new Cubo();
						VerticeMC[] vertices = new VerticeMC[8];
						vertices[0] = new VerticeMC(i,j,k);					
						vertices[1] = new VerticeMC(i,j+1,k);
						vertices[2] = new VerticeMC(i+1,j+1,k);
						vertices[3] = new VerticeMC(i+1,j,k);
						vertices[4] = new VerticeMC(i,j,k+1);
						vertices[5] = new VerticeMC(i,j+1,k+1);
						vertices[6] = new VerticeMC(i+1,j+1,k+1);
						vertices[7] = new VerticeMC(i+1,j,k+1);
						for(int w=0; w<8; w++){
//							vertices[w].calcPeso(w,h,l,N,fatias);// etValor(valorPeso(vertices[w]));			
							vertices[w].setValor(valorPeso(vertices[w]));
						}
						cubo.setVertices(vertices);
						cubo.poligonizar(new Double(isoValue));

						if(cubo.getTriangulos() != null){
							cubos.add(cubo);
						}
						
					}
				}
			}
			fatias = null;
			System.out.println("End of MC. Cubes: "+cubos.size());
			
			
			ArrayList<WB_Triangle> wbTriangles = new ArrayList<WB_Triangle>();
			
			// Cubes --> Triangles
			Iterator<Cubo> it = cubos.iterator();
			while(it.hasNext()){
				Cubo cubo = it.next();
				if(cubo.getTriangulos()!=null){
					//triangulos.addAll(cubo.getTriangulos());
					
					for(Triangle t : cubo.getTriangulos()){
						//HE_Face face = new HE_Face();
						
						/*mesh.add(new HE_Vertex(t.p[0].x,t.p[0].y,t.p[0].z));
						mesh.add(new HE_Vertex(t.p[1].x,t.p[1].y,t.p[1].z));
						mesh.add(new HE_Vertex(t.p[2].x,t.p[2].y,t.p[2].z));*/
					
											
						WB_Point p1 = new WB_Point(t.p[0].x,t.p[0].y,t.p[0].z*2);
						WB_Point p2 = new WB_Point(t.p[1].x,t.p[1].y,t.p[1].z*2);
						WB_Point p3 = new WB_Point(t.p[2].x,t.p[2].y,t.p[2].z*2);
					    
						WB_Triangle tri = new WB_Triangle(p1,p2,p3);
						wbTriangles.add(tri);
					}
				}
			}
			//System.err.println(triangulos.size());
			System.err.println("Triangles count: "+wbTriangles.size());
			
			//HEC_FromBinarySTLFile fromStl = new HEC_FromBinarySTLFile();

			HEC_FromTriangles fromPolygons = new HEC_FromTriangles();
			fromPolygons.setTriangles(wbTriangles);
			
			mesh = fromPolygons.create();
			if(mesh.validate()) System.out.println("Mesh OK!");
			
			size = wbTriangles.size() * 9;
			wbTriangles.clear();

			
			System.out.println("Smoothing...");
			
//			SDSmooth smooth = new SDSmooth();
//			smooth.scaleDependentSmoothing(mesh, 4, 0.50376, false);
			
			
			SDLSmooth modifier = new SDLSmooth();
			modifier.setStepSize(0.18756f);
			//modifier.setIterations(12);
			modifier.setIterations(2);
			modifier.setKeepBoundary(false);
			modifier.setAutoRescale(true);// rescale mesh to original extents
			mesh.modify(modifier);
			
			System.out.println("Uouu! It was beautiful!");
			  
			
			//HET_Export.saveToSTL(mesh, "/Users/emoretto/","box2");
			
			triangle = Buffers.newDirectFloatBuffer(size*9);
			normal = Buffers.newDirectFloatBuffer(size*9);
			int count = 0;
			
			for(WB_Triangle t : mesh.getTriangles()){
				count++;
				
				triangle.put(t.p1().xf());
				triangle.put(t.p1().yf());
				triangle.put(t.p1().zf());
				
				triangle.put(t.p2().xf());
				triangle.put(t.p2().yf());
				triangle.put(t.p2().zf());
				
				triangle.put(t.p3().xf());
				triangle.put(t.p3().yf());
				triangle.put(t.p3().zf());

				//Normal recalculation - cuz smooth destroy normals
				Vector3f vec0 = new Vector3f(t.p1().xf(), t.p1().yf(), t.p1().zf());
				Vector3f vec1 = new Vector3f(t.p2().xf(), t.p2().yf(), t.p2().zf());
				Vector3f vec2 = new Vector3f(t.p3().xf(), t.p3().yf(), t.p3().zf());
		    	
				vec1.sub(vec0);    	
		    	vec2.sub(vec0);
		    	Vector3f vecNorm = new Vector3f(0.0f, 0.0f, 0.0f);
		    	vecNorm.cross(vec1, vec2);
		    	vecNorm.normalize();
				
				normal.put(vecNorm.x);
				normal.put(vecNorm.y);
				normal.put(vecNorm.z);
				
				normal.put(vecNorm.x);
				normal.put(vecNorm.y);
				normal.put(vecNorm.z);
				
				normal.put(vecNorm.x);
				normal.put(vecNorm.y);
				normal.put(vecNorm.z);
				
				/*t.getPlane().getNormal().xf());
				normal.put(t.getPlane().getNormal().yf());
				normal.put(t.getPlane().getNormal().zf());
				
				normal.put(t.getPlane().getNormal().xf());
				normal.put(t.getPlane().getNormal().yf());
				normal.put(t.getPlane().getNormal().zf());
				
				normal.put(t.getPlane().getNormal().xf());
				normal.put(t.getPlane().getNormal().yf());
				normal.put(t.getPlane().getNormal().zf());*/
			}
			System.out.println("ESSE "+count);
			
			/*			
			count = 0;
			for(Triangle t : triangulos){
				count++;
				triangle2.put(t.p[0].x);
				triangle2.put(t.p[0].y);
				triangle2.put(t.p[0].z*zSpace);
				
				triangle2.put(t.p[1].x);
				triangle2.put(t.p[1].y);
				triangle2.put(t.p[1].z*zSpace);
				
				triangle2.put(t.p[2].x);
				triangle2.put(t.p[2].y);
				triangle2.put(t.p[2].z*zSpace);
				
				
				normal.put(t.vecNorm.x);
				normal.put(t.vecNorm.y);
				normal.put(t.vecNorm.z);
				
				normal.put(t.vecNorm.x);
				normal.put(t.vecNorm.y);
				normal.put(t.vecNorm.z);
				
				normal.put(t.vecNorm.x);
				normal.put(t.vecNorm.y);
				normal.put(t.vecNorm.z);
			}
			System.out.println("ESSE2 "+count);
			*/
	        System.out.println("EOF");
			
		} catch (Exception e1) {
			e1.printStackTrace();
		}
    	
    	
    	Renderer me = new Renderer();
    	
        GLProfile glprofile = GLProfile.getDefault();
        GLCapabilities glcapabilities = new GLCapabilities( glprofile );
        GLCanvas canvas = new GLCanvas( glcapabilities );
        
        Frame frame = new Frame("Visualizador 3D");
        frame.setSize(_WINDOW_WIDTH, _WINDOW_HEIGHT);
        frame.add(canvas);
        frame.setResizable(false);
        frame.setVisible(true);
        
        
        // by default, an AWT Frame doesn't do anything when you click
        // the close button; this bit of code will terminate the program when
        // the window is asked to close
        frame.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        });
        
        //Mouse Handler
        MouseHandler inputHandler = new MouseHandler(me);        
        
        canvas.addGLEventListener(me);
        canvas.addMouseListener(inputHandler);
        canvas.addMouseMotionListener(inputHandler);
        canvas.addMouseWheelListener(inputHandler);
        
        // Esse �� o inferno que fica fazendo chamar o display por fps - precisa!
        animator = new FPSAnimator(canvas, 90);
        animator.setUpdateFPSFrames(3, null);
        animator.start();
    }
    
    
    public static double valorPeso(VerticeMC v){
    	
		int ix = (int)((v.x/w)*N[0]);
		int iy = (int)((v.y/h)*N[1]);
		int iz = (int)((v.z/l)*N[2]);
		double fx = (v.x/w)*N[0] - ix;
		double fy = (v.y/h)*N[1] - iy;
		double fz = (v.z/l)*N[2] - iz;
		double A = 0;
		double B = 0;
		double C = 0;
		double D = 0;
		double E = 0;
		double F = 0;
		double G = 0;
		
		
		if(ix<N[0] && iy<N[1] && iz<N[2]){
			if(ix+1<N[0]){
				A = (1-fx)*fatias[ix][iy][iz] + fx*fatias[ix+1][iy][iz];		
				if(iy+1<N[1]){
					B= (1-fx)*fatias[ix][iy+1][iz] + fx*fatias[ix+1][iy+1][iz];
					if(iz+1<N[2]){
						D= (1-fx)*fatias[ix][iy+1][iz+1] + fx*fatias[ix+1][iy+1][iz+1]; 
					}
				}
				if(iz+1<N[2]){
					C= (1-fx)*fatias[ix][iy][iz+1] + fx*fatias[ix+1][iy][iz+1]; 
				}
			}
		}
		E=(1-fy)*A+fy*B;
		F=(1-fy)*C+fy*D;
		G=(1-fz)*E+fz*F;
		return G;
	}
    

    public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height) {
    	GL2 gl = drawable.getGL().getGL2();

        gl.glViewport(0, 0, width, height);                // Reset The Current Viewport
        gl.glMatrixMode(GL2.GL_PROJECTION);                                        // Select The Projection Matrix
        gl.glLoadIdentity();                                                    // Reset The Projection Matrix
        //gl.glOrthof(-10f, 10f, -10f, 10f, 0f, 50f);
        glu.gluPerspective(45.0f, (float) (width) / (float) (height),           // Calculate The Aspect Ratio Of The Window
                1.0f, 100.0f);
        gl.glMatrixMode(GL2.GL_MODELVIEW);                                        // Select The Modelview Matrix
        gl.glLoadIdentity();                                                    // Reset The Modelview Matrix

        arcBall.setBounds((float) width, (float) height);                 //*NEW* Update mouse bounds for arcball
    }
    public void displayChanged(GLAutoDrawable drawable, boolean modeChanged, 
            boolean deviceChanged) {
        init(drawable);
    }
 
    public void DoFog( GL2 gl )
    {
        int FOG_DISTANCE = 90;
        
        float[] fogcol =    { 0, 0, 0, 0 };
        
        gl.glEnable(GL2.GL_FOG);
        gl.glFogi(GL2.GL_FOG_MODE, GL.GL_LINEAR);
        gl.glFogf(GL2.GL_FOG_DENSITY, 0f);
        gl.glFogfv(GL2.GL_FOG_COLOR, fogcol, 0);
        gl.glFogf(GL2.GL_FOG_START, FOG_DISTANCE - 30);
        gl.glFogf(GL2.GL_FOG_END, FOG_DISTANCE);
        gl.glHint(GL2.GL_FOG_HINT, GL.GL_NICEST);
    }
    
    
    public void init(GLAutoDrawable drawable) {
    	GL2 gl = drawable.getGL().getGL2();

        // Start Of User Initialization
        LastRot.setIdentity();                                // Reset Rotation
        ThisRot.setIdentity();                                // Reset Rotation
        ThisRot.get(matrix);

        gl.glClearColor(0.0f, 0.0f, 0.0f, 0.5f);                            // Black Background
        gl.glClearDepth(1.0f);                                            // Depth Buffer Setup
        gl.glDepthFunc(GL.GL_LEQUAL);                                        // The Type Of Depth Testing (Less Or Equal)
        gl.glEnable(GL.GL_DEPTH_TEST);                                        // Enable Depth Testing
        //gl.glShadeModel(GL2.GL_FLAT);                                            // Select Flat Shading (Nice Definition Of Objects)
        gl.glHint(GL2.GL_PERSPECTIVE_CORRECTION_HINT, GL.GL_NICEST);                // Set Perspective Calculations To Most Accurate

        quadratic = glu.gluNewQuadric();                                        // Create A Pointer To The Quadric Object
        glu.gluQuadricNormals(quadratic, GLU.GLU_SMOOTH);                        // Create Smooth Normals
        glu.gluQuadricTexture(quadratic, true);                            // Create Texture Coords

//        gl.glEnable(GL2.GL_LIGHT1); // Enable Default Light
        gl.glEnable(GL2.GL_LIGHTING);                                            // Enable Lighting
        gl.glEnable(GL2.GL_LIGHT0);  

        gl.glEnable(GL2.GL_CULL_FACE);
        gl.glCullFace(GL.GL_BACK);
        
        float posLight1[] = { 0.0f, 3.0f, 10.0f, 1.0f };
        float spotDirection[] = { 0.0f, 0.0f, -1.0f };
        
        //
        gl.glLightfv( GL2.GL_LIGHT0, GL2.GL_POSITION, posLight1 ,0);
        gl.glLightfv( GL2.GL_LIGHT0, GL2.GL_SPOT_DIRECTION, spotDirection, 0);
        gl.glLightfv( GL2.GL_LIGHT0, GL2.GL_AMBIENT, colorAmbient, 0);
        gl.glLightfv( GL2.GL_LIGHT0, GL2.GL_DIFFUSE, colorGray, 0 );
//        gl.glLightfv( GL2.GL_LIGHT0, GL2.GL_SPECULAR, colorWhite, 0 );
        //gl.glLightfv( GL.GL_LIGHT1, GL.GL_SPECULAR, colorRed );
        //
        

       
//		gl.glGenBuffers(nbVBO, VBO, 1);
        gl.glGenBuffers(2, buffers);
		
		// Enable same as for vertex buffers.
		 gl.glEnableClientState( GL2.GL_VERTEX_ARRAY );
		 gl.glEnableClientState( GL2.GL_NORMAL_ARRAY );
		 
		 
		// Init VBOs and transfer data.
		 gl.glBindBuffer( GL2.GL_ARRAY_BUFFER, buffers.get(0) );
		 
		 triangle.rewind();
		 normal.rewind();
		 
		 // Copy data to the server into the VBO.
		 gl.glBufferData( GL2.GL_ARRAY_BUFFER,
		                     size*9, triangle,
		                     GL2.GL_STATIC_DRAW );
		 
		 gl.glVertexPointer( 3, GL.GL_FLOAT, 0, 0 );
		 

		 
		 //Normal
		 
		// Init VBOs and transfer data.
		 gl.glBindBuffer( GL2.GL_ARRAY_BUFFER, buffers.get(1) );
		 
		 // Copy data to the server into the VBO.
		 gl.glBufferData( GL2.GL_ARRAY_BUFFER,
		                     size*9, normal,
		                     GL2.GL_STATIC_DRAW );
	        
		 gl.glNormalPointer( GL.GL_FLOAT, 0, 0 );
		 
		/*
        
        gl.glGenBuffers(2, buffers);
        
        gl.glEnableClientState(GL2.GL_VERTEX_ARRAY);
        //gl.glEnableClientState(GL2.GL_NORMAL_ARRAY);
        triangle.rewind();
        
        gl.glBindBuffer(GL2.GL_ARRAY_BUFFER, buffers.get(0));
        gl.glVertexPointer(3, GL2.GL_FLOAT, 0, buffers.get(0));
        gl.glBufferData(GL2.GL_ARRAY_BUFFER, size  * 9, triangle, GL2.GL_STATIC_DRAW);

        
        gl.glBindBuffer(GL2.GL_ARRAY_BUFFER, buffers.get(1));
        gl.glNormalPointer(GL2.GL_FLOAT, 0, buffers.get(1));
        gl.glBufferData(GL2.GL_ARRAY_BUFFER, size * 9, normal, GL2.GL_STATIC_DRAW);

        gl.glEnableVertexAttribArray(0);
        */
        
    }
    
    void reset() {
        synchronized(matrixLock) {
            LastRot.setIdentity();   // Reset Rotation
            ThisRot.setIdentity();   // Reset Rotation
        }
    }

    void startDrag( Point MousePt ) {
        synchronized(matrixLock) {
            LastRot.set( ThisRot );                                        // Set Last Static Rotation To Last Dynamic One
        }
        arcBall.click( MousePt );                                 // Update Start Vector And Prepare For Dragging
    }
 
    void drag( Point MousePt )       // Perform Motion Updates Here
    {
        Quat4f ThisQuat = new Quat4f();
 
        // Update End Vector And Get Rotation As Quaternion
        arcBall.drag( MousePt, ThisQuat); 
        synchronized(matrixLock) {
            ThisRot.setRotation(ThisQuat);  // Convert Quaternion Into Matrix3fT
            ThisRot.mul( ThisRot, LastRot); // Accumulate Last Rotation Into This One
        }
    }
 
    void zoom(int value){
   		zoom += ((float) value)/8f;
    }
    
    void renderStrokeString(GL2 gl, int font, String string) {
        // Center Our Text On The Screen
        float width = glut.glutStrokeLength(font, string);
//        gl.glTranslatef(-width / 2f, 0, 0);
        // Render The Text
        for (int i = 0; i < string.length(); i++) {
            char c = string.charAt(i);
            glut.glutStrokeCharacter(font, c);
        }
    }
    
    
    @Override
    public void display(GLAutoDrawable drawable) {
    	
    	synchronized(matrixLock) {
            ThisRot.get(matrix);
        }
    	
        GL2 gl = drawable.getGL().getGL2();
 
        gl.glMatrixMode(GL2.GL_PROJECTION);                                        // Select The Projection Matrix
        gl.glLoadIdentity();                                                    // Reset The Projection Matrix
        //gl.glOrthof(-10f, 10f, -10f, 10f, 0f, 50f);
        glu.gluPerspective(45.0f, (float) (640) / (float) (480),           // Calculate The Aspect Ratio Of The Window
                1.0f, 100.0f);
        gl.glMatrixMode(GL2.GL_MODELVIEW);     
        
        // Clear Screen And Depth Buffer
        gl.glClear(GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT);                
         
   
        gl.glLoadIdentity();   
        
        gl.glPolygonMode( GL2.GL_FRONT, GL2.GL_FILL );
        //gl.glPolygonMode( GL2.GL_FRONT_AND_BACK, GL2.GL_LINE );
        
//        DoFog(gl);
        
        // Move Right 1.5 Units And Into The Screen 7.0
        gl.glTranslatef(0.0f, 0.0f, -10.0f + zoom);  
 
        gl.glPushMatrix();                  // NEW: Prepare Dynamic Transform
        //Aqui est�� sem a rotacao - bom pra fazer um fundo em perspectiva
        gl.glMultMatrixf(matrix, 0);        // NEW: Apply Dynamic Transform
        

        //Esfera DEBUG
        glu.gluSphere(quadratic, 0.3f, 20, 20);
        
        gl.glDisable(GL2.GL_COLOR_MATERIAL);  
        //gl.glEnable(GL2.GL_LigOLOR_MATERIAL);  

        float[] rgba = {0.9f, 0.6f, 0.4f, 0.2f};
    	gl.glMaterialfv(GL.GL_FRONT, GL2.GL_DIFFUSE, rgba, 0);
    	
        /*float[] rgbaSpec = {1.0f, 1.0f, 1.0f, 1.0f};
        gl.glMaterialfv(GL.GL_FRONT, GL2.GL_SPECULAR, rgbaSpec, 0);
    	gl.glMaterialf(GL.GL_FRONT, GL2.GL_SHININESS, .51f);*/

    	/* DEBUG
    	Vector3f vec0 = new Vector3f(0.0f, 1.0f, 0.0f);
    	Vector3f vec1 = new Vector3f(-1.0f, -1.0f, 0.0f);
    	Vector3f vec2 = new Vector3f(1.0f, -1.0f, 0.0f);
    	Vector3f vecNorm = new Vector3f(0.0f, 0.0f, 0.0f);
   	
    	calcNormal(vec0, vec1, vec2, vecNorm);
    	gl.glNormal3f(vecNorm.x,vecNorm.y,vecNorm.z);
    	
        gl.glBegin(GL.GL_TRIANGLES);
        gl.glVertex3f(0.0f, 1.0f, 0.0f);  // Top
        gl.glVertex3f(-1.0f, -1.0f, 0.0f);  // Bottom Left
        gl.glVertex3f(1.0f, -1.0f, 0.0f);  // Bottom Right
        gl.glEnd();        // Finished Drawing The Triangle
        
        
        
        Vector3f vec20 = new Vector3f(1.0f, 2.0f, -1.0f);
    	Vector3f vec21 = new Vector3f(0.0f, 1.0f, 0.0f);
    	Vector3f vec22 = new Vector3f(1.0f, -1.0f, 0.0f);
    	Vector3f vec2Norm = new Vector3f(0.0f, 0.0f, 0.0f);
   	
    	calcNormal(vec20, vec21, vec22, vec2Norm);
    	gl.glNormal3f(vec2Norm.x,vec2Norm.y,vec2Norm.z);
        gl.glBegin(GL.GL_TRIANGLES);    // Drawing Using Triangles
        gl.glVertex3f(1.0f, 2.0f, -1.0f);  // Bottom Right
        gl.glVertex3f(0.0f, 1.0f, 0.0f);  // Bottom Left
        gl.glVertex3f(1.0f, -1.0f, 0.0f);  // Top
        gl.glEnd();        // Finished Drawing The Triangle
        
        */
        //gl.glEnable(GL2.GL_COLOR_MATERIAL);  
        //crosshair(gl);   

        
        //centralizando o modelo
        //habilitar esfera acima para debug
        gl.glTranslatef(-2.2f, -4.5f, -3f); 
        
        
        
       // gl.glNormalPointer(GL2.GL_FLOAT, 0, normal);
        //gl.glDisableClientState(GL2.GL_VERTEX_ARRAY);
        
        /*
        gl.glVertexPointer(3, GL2.GL_FLOAT, 0, normal);
        gl.glDisableClientState(GL2.GL_NORMAL_ARRAY);
        gl.glDrawArrays(GL2.GL_TRIANGLES, 0, 3);
        */

        
     // Draw.
        gl.glDrawArrays( GL.GL_TRIANGLES, 0, size*3 );

        
        gl.glEnable( GL.GL_POLYGON_OFFSET_FILL );      
        gl.glPolygonOffset( 1f, 1f );

        
        
        float[] rgbaWire = {0.8f, 0.5f, 0.3f, 0.2f};
		
        gl.glPolygonMode( GL2.GL_FRONT, GL2.GL_LINE );        
        gl.glMaterialfv(GL.GL_FRONT, GL2.GL_DIFFUSE, rgbaWire, 0);
        
        gl.glEnable( GL2.GL_POLYGON_OFFSET_LINE );
        gl.glPolygonOffset( 2.0f, 2.0f );
        
        gl.glDrawArrays( GL.GL_TRIANGLES, 0, size*3 );
        
        gl.glDisable( GL2.GL_POLYGON_OFFSET_LINE );   
        
        /*
         * 
        // Projetando os triangulos
        gl.glBegin(GL.GL_TRIANGLES);

        for(Triangle tri : triangulos){

			gl.glNormal3f(tri.vecNorm.x,tri.vecNorm.y,tri.vecNorm.z);
			gl.glVertex3f(tri.p[0].x, tri.p[0].y, tri.p[0].z*2f);
			gl.glVertex3f(tri.p[1].x, tri.p[1].y, tri.p[1].z*2f);
			gl.glVertex3f(tri.p[2].x, tri.p[2].y, tri.p[2].z*2f);
				
		}
		gl.glEnd(); //end GL_TRIANGLES
		
		
		
	       // Projetando os triangulos - WIREFRAME
		float[] rgbaWire = {0.8f, 0.5f, 0.3f, 0.2f};
		gl.glPolygonMode( GL2.GL_FRONT, GL2.GL_LINE );        
        //gl.glEnable(GL2.GL_COLOR_MATERIAL);  
//        gl.glMaterialfv(GL.GL_FRONT, GL2.GL_DIFFUSE, rgbaWire, 0);
        
        gl.glEnable(GL2.GL_COLOR_MATERIAL);  
        
        
        gl.glBegin(GL.GL_TRIANGLES);
        gl.glColor3fv(rgbaWire,0);
        for(Triangle tri : triangulos){

			//gl.glNormal3f(tri.vecNorm.x,tri.vecNorm.y,tri.vecNorm.z);
			gl.glVertex3f(tri.p[0].x, tri.p[0].y, tri.p[0].z*2f);
			gl.glVertex3f(tri.p[1].x, tri.p[1].y, tri.p[1].z*2f);
			gl.glVertex3f(tri.p[2].x, tri.p[2].y, tri.p[2].z*2f);
				
		}
		gl.glEnd(); //end GL_TRIANGLES
		*/
        
		
        //Zerando a projecao e mudando pra modelview (2D)

        gl.glMatrixMode(GL2.GL_PROJECTION);    
        gl.glLoadIdentity();


        gl.glMatrixMode(GL2.GL_MODELVIEW);    
        gl.glLoadIdentity();

        
        gl.glTranslatef(-0.99f, 0.95f, 0f);
        gl.glScalef(0.0002f, 0.0002f, 1f);
        gl.glColor3f(1.0f, 0.0f, 0.0f);
        renderStrokeString(gl, GLUT.STROKE_MONO_ROMAN, "FPS "+animator.getLastFPS() );
        
        //Text
        /*
        //X
        gl.glTranslatef(-2.5f, -3.0f, 0f);
        gl.glScalef(0.002f, 0.002f, 1f);
        gl.glColor3f(1.0f, 0.0f, 0.0f);
        renderStrokeString(gl, GLUT.STROKE_MONO_ROMAN, "X" );
        
        //y
        gl.glTranslatef(-2.5f, -3.0f, 0f);
        gl.glScalef(0.002f, 0.002f, 1f);
        gl.glColor3f(1.0f, 0.0f, 0.0f);
        renderStrokeString(gl, GLUT.STROKE_MONO_ROMAN, "y" );
        */
        
        gl.glPopMatrix();                   // NEW: Unapply Dynamic Transform
        gl.glFlush();                       // Flush The GL Rendering Pipeline
    }

	

	private void crosshair(GL2 gl) {
		
        gl.glBegin(GL.GL_LINES);    // X
        gl.glColor3f(1.0f, 0.0f, 0.0f);
        gl.glVertex2f(-3.5f, -3.0f);
        gl.glVertex2f(-2.5f, -3.0f);
        gl.glEnd(); 
        
        gl.glBegin(GL.GL_LINES);    // Y
        gl.glColor3f(0.0f, 1.0f, 0.0f);
        gl.glVertex2f(-3.5f, -3.0f);
        gl.glVertex2f(-3.5f, -2.0f);
        gl.glEnd();   
        
        gl.glBegin(GL.GL_LINES);    // Z
        gl.glColor3f(0.0f, 0.0f, 1.0f);
        gl.glVertex3f(-3.5f, -3.0f, 0f);
        gl.glVertex3f(-3.5f, -3.0f, 1f);
        gl.glEnd();
	}

	@Override
	public void dispose(GLAutoDrawable drawable) {
	}
}