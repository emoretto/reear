/*
 * 
 */
package br.eng.moretto.earmaker.gl3d;

import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import javax.vecmath.Vector3f;

import wblut.geom.WB_AABB;
import wblut.geom.WB_Point;
import wblut.hemesh.HEM_Modifier;
import wblut.hemesh.HE_Mesh;
import wblut.hemesh.HE_Selection;
import wblut.hemesh.HE_Vertex;

/**
 * 
 * Scale Dependent Laplace Smooth - Fujiwara Operator
 * custom version by emoretto at gmail
 * 
 * ref: https://searchcode.com/codesearch/view/28445471/
 * 
 */
public class SDLSmooth extends HEM_Modifier {
    
    private boolean autoRescale = true;
    
    private boolean keepBoundary = true;
    
    private int iter = 1;
    
    private float stepSize = 0.1f;

	public SDLSmooth() {
	}
    
    public SDLSmooth(boolean autoRescale, boolean keepBoundary, int iter,
			float stepSize) {
		super();
		this.autoRescale = autoRescale;
		this.keepBoundary = keepBoundary;
		this.iter = iter;
		this.stepSize = stepSize;
	}

	public SDLSmooth setAutoRescale(final boolean b) {
    	autoRescale = b;
    	return this;
    }

    public SDLSmooth setIterations(final int r) {
    	iter = r;
    	return this;
    }

    public SDLSmooth setKeepBoundary(final boolean b) {
    	keepBoundary = b;
    	return this;
    }

	@Override
	public HE_Mesh apply(final HE_Mesh mesh) {
		
		final WB_Point[] newPositions = new WB_Point[mesh.getNumberOfVertices()];
		
//		tracker.setDefaultStatus("Starting HEM_Smooth.");
		WB_AABB box = new WB_AABB();
		if (autoRescale)
			box = mesh.getAABB();
		
//		tracker.setDefaultStatus("Smoothing vertices.", iter * mesh.getNumberOfVertices());
		for (int r = 0; r < iter; r++) {

			System.out.println("Iter "+r+"/"+iter);
			
			Iterator<HE_Vertex> vItr = mesh.vItr();
			HE_Vertex v;
			
			int id = 0;
			while (vItr.hasNext()) {
				v = vItr.next();
				
				// So vamos aplicar o metodo em vertices que nao sao bordas (ou limites)
				if (v.isBoundary() && keepBoundary) {
					newPositions[id] = v.getPoint();	 //new WB_Point(v.get().xd(),v.get().yd(),v.get().zd()); // v.getPoint();					
				} else {

					//Operador laplaciano com peso dividido igualmente entre todos os vertices
					WB_Point p = new WB_Point(v);
					
					List<HE_Vertex> neighbors = v.getNeighborVertices();
					WB_Point weightedPoint = new WB_Point();
					Vector<Float> weights = new Vector<Float>();
					int weightsIndex = 0;
					
					//Calculo do peso entre todos os vertices, em funcao da distancia do vertice
					for (int i = 0; i < neighbors.size(); i++) {
						
						WB_Point neighborPoint = neighbors.get(i).getPoint();
						WB_Point npDiff = neighborPoint.sub(p);
						
						//peso calculado em funcao da distancia do vertice!!!!!!
						float weight = 1.0f / (float) npDiff.getLength3D();

						npDiff.mulSelf(weight);
						weightedPoint.addSelf(npDiff);

						weights.add(weightsIndex,weight);
						weightsIndex++;
					}
					
					float sum = 0;
					for(float w : weights)
						sum += w;
					
					weightedPoint.divSelf(sum);
					
					p.addSelf(weightedPoint);
					
					newPositions[id] = p.scaleSelf(stepSize); 
//					newPositions[id] = p;
					
				}
				id++;
			}
			vItr = mesh.vItr();
			id = 0;
			while (vItr.hasNext()) {
				vItr.next().set(newPositions[id]);
				id++;
//				tracker.incrementCounter();
			}
		}
		mesh.resetCenter();
		if (autoRescale) {
			mesh.fitInAABB(box);
		}
//		tracker.setDefaultStatus("Exiting HEM_Smooth.");
		return mesh;
	}

	private Vector3f toVector3f(HE_Vertex n) {
		return new Vector3f(n.xf(),n.yf(),n.zf());
	}

    /*
     * Apagado - nao vamos precisar disso 
     */
    @Override
    public HE_Mesh apply(final HE_Selection selection) {
    	return null;
    }
    public float getStepSize() {
  		return stepSize;
  	}

  	public void setStepSize(float stepSize) {
  		this.stepSize = stepSize;
  	}
}