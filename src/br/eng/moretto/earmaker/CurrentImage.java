package br.eng.moretto.earmaker;

import ij.ImagePlus;
import ij.measure.Calibration;
import ij.plugin.DICOM;

import java.awt.image.BufferedImage;
import java.util.SortedMap;

import com.pixelmed.dicom.AttributeList;
import com.pixelmed.dicom.DicomException;
import com.pixelmed.dicom.TagFromName;

public class CurrentImage {

	public ImagePlus imagePlus;
	public DICOM dicom;
	public Calibration calibration;
	public SortedMap<Float, BufferedImage> set;
	public SortedMap<Integer, AttributeList> setDicomInfo;
	
	public double pixelSpacing = 0d;
	public double sliceThickness = 0d;
	public double sliceSpacing = 0d;

	// Qual indice esta apontando para qual camada
	public SortedMap<Integer, Float> index;
	
	public CurrentImage(ImagePlus imagePlus, DICOM dicom,Calibration calibration) {
		this.imagePlus = imagePlus;
		this.dicom = dicom;
		this.calibration = calibration;
	}

	public CurrentImage() {
	}
	
	
	public float getRescaleSlope(){	
		try {
			return setDicomInfo.get( imagePlus.getSlice()-1 ).get(TagFromName.RescaleSlope).getFloatValues()[0];
		} catch (DicomException e) {
			e.printStackTrace();
			return 1.0f;
		}
	}
	
	public float getRescaleIntercept(){	
		try {
			return setDicomInfo.get( imagePlus.getSlice()-1 ).get(TagFromName.RescaleIntercept).getFloatValues()[0];
		} catch (DicomException e) {
			e.printStackTrace();
			return 1.0f;
		}
	}
}
