package br.eng.moretto.earmaker.dicom;

import ij.ImagePlus;
import ij.ImageStack;
import ij.plugin.DICOM;
import ij.util.DicomTools;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.SortedMap;
import java.util.TreeMap;

import loci.formats.FormatException;
import br.eng.moretto.earmaker.CurrentImage;
import br.eng.moretto.earmaker.Main;

import com.pixelmed.dicom.Attribute;
import com.pixelmed.dicom.AttributeList;
import com.pixelmed.dicom.AttributeTag;
import com.pixelmed.dicom.DicomException;
import com.pixelmed.dicom.DicomInputStream;
import com.pixelmed.dicom.TagFromName;
import com.pixelmed.display.SourceImage;

public class PixelMed {
	
	
	private static String getTagInformation(AttributeTag attrTag, AttributeList list) {
		return Attribute.getDelimitedStringValuesOrEmptyString(list, attrTag);
	}
		
	long map(long x, long in_min, long in_max, long out_min, long out_max)
	{
	  return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
	}
	
	public static CurrentImage run() throws FormatException, IOException, DicomException {
		
		//JFileChooser fc = new JFileChooser();
		//fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		//if(fc.showDialog(null, "Abrir") == JFileChooser.APPROVE_OPTION)
		{
			//File file = fc.getSelectedFile();
		
		CurrentImage ci = new CurrentImage();
			
		//File file = new File("/Users/emoretto/workspace.mestrado/earmaker/resources/kss_dicom2/Kaique_Da_Silva_Santos");
		File file = new File("/Users/emoretto/workspace.mestrado/earmaker/resources/DICOM/S467300/S20");

		
//		File file = new File("/Volumes/120gb/DICOM - Reinaldo Lopes/DICOM Exported/RL/");
		//File file = new File("/Volumes/120gb/DICOM - Vitor Ribeiro - Partes Moles/Ribeiro_Vitor_K_S_20050914_5295504_Tc_Ossos_Temporais_252_4462879_20120824/");
			
			if(file.isDirectory()){
			
				// Map para armazenar os arquivos ordenadamente
				SortedMap<Float, BufferedImage> set = new TreeMap<Float, BufferedImage>();
				//SortedMap<Float, SourceImage> setSource = new TreeMap<Float, SourceImage>();
				SortedMap<Integer, Float> setIndex = new TreeMap<Integer, Float>();
				
				SortedMap<Float, AttributeList> setDicomInfoTemp = new TreeMap<Float, AttributeList>();
				SortedMap<Integer, AttributeList> setDicomInfo = new TreeMap<Integer, AttributeList>();
				
				int width=0;
				int height=0;
				DICOM dicomInfo = null;
				
				int count=0;
				
				for(File f : file.listFiles()){
					
					System.out.println(count+" "+ f.getName());
					
					count++;
					
					//if(count < 52 || count > 162 )
					//if(count < 52 || count > 140 )
					//if(count < 59 || count > 160 )
					//	continue;
					
					//if(count > 10) break;					
					//if(count != 129 && count != 128 && count != 127  && count != 126  && count != 125  && count != 124  && count != 123  && count != 122  && count != 121 ) continue;
										
					
					// Lendo atributos do DICOM
					DicomInputStream i = new DicomInputStream(f);
					AttributeList list = new AttributeList();
					list.read(i);				
					i.close();		
					
	
					// Utilizamos a SliceLocation como indexador da ordem do Stack
					Attribute a = list.get(TagFromName.SliceLocation);
					
					// Se não há info, não é arquivo DICOM
					if(a == null) continue;
	
					
					// Lendo atributos pela classe DICOM do IJ - mais fácil
					if(dicomInfo == null)
					{
						
						Main.console("Dicom info:");
						System.out.println("Dicom info:");
						dicomInfo = new DICOM();
						dicomInfo.open(f.getPath());
						
						/** 
						 * Hounsfield Unit - hu = pixel_value * slope + intercept
						*/
						try{
							System.out.println("RescaleIntercept " + list.get(TagFromName.RescaleIntercept).getFloatValues()[0]);
							System.out.println("RescaleSlope " + list.get(TagFromName.RescaleSlope).getFloatValues()[0]);
	
						}catch(Exception e){
							System.err.println("Erro ao pegar os valores RescaleIntercept e RescaleSlope - utilizando 1 para ambos");
							e.printStackTrace();
						}
						
						/** 						 * 
						 * Gantry/Detector Tilt (0018,1120) for NM Image data is the angle in degrees of the detector face relative to the patient's major (Head to Feet) axis (or the table supporting the patient). Positive tilt is towards the patient’s feet.
						 */
						try{
							System.out.println("IPP " + list.get(TagFromName.ImagePositionPatient).getFloatValues()[0]);
							System.out.println("Gantry Detector Tilt " + list.get(TagFromName.GantryDetectorTilt).getFloatValues()[0]);
							System.out.println("Slice Thickness " + list.get(TagFromName.SliceThickness).getFloatValues()[0]);
							System.out.println("Spacing between slices: "+list.get(TagFromName.SpacingBetweenSlices).getFloatValues()[0]);
							System.out.println("Pixel spacing: "+list.get(TagFromName.PixelSpacing).getFloatValues()[0]);
							
							ci.pixelSpacing = list.get(TagFromName.PixelSpacing).getFloatValues()[0];
							ci.sliceThickness = list.get(TagFromName.SliceThickness).getFloatValues()[0];
							ci.sliceSpacing = list.get(TagFromName.SpacingBetweenSlices).getFloatValues()[0];
						
						}catch(Exception e){}
						
						height = list.get(TagFromName.Rows).getIntegerValues()[0];
						width = list.get(TagFromName.Columns).getIntegerValues()[0];
	
						Main.console("Modality: " + getTagInformation(TagFromName.Modality, list));
						Main.console("Width: "+DicomTools.getTag(dicomInfo, "0028,0010"));
						Main.console("Height: "+DicomTools.getTag(dicomInfo, "0028,0011"));
						Main.console("Slice thickness: "+DicomTools.getTag(dicomInfo, "0018,0050"));
						Main.console("Spacing between slices: "+DicomTools.getTag(dicomInfo, "0018,0088"));
						Main.console("Pixel spacing: "+DicomTools.getTag(dicomInfo, "0028,0030"));
						Main.console("Image Position Pat: "+DicomTools.getTag(dicomInfo, "0020,0032"));				
						Main.console("Gantry Tilt: "+DicomTools.getTag(dicomInfo, "0018,1120"));
						Main.console("Gantry Detector Tilt: "+list.get(TagFromName.GantryDetectorTilt).getFloatValues()[0]);
						 

						Main.console("Bits Allocated: " + getTagInformation(TagFromName.BitsAllocated, list));
						Main.console("Bits Stored: " + getTagInformation(TagFromName.BitsStored, list));
						Main.console("High Bit:" + getTagInformation(TagFromName.HighBit, list));
						Main.log("Loading...");						
					}
	
					//Dicom Info - TEMP
					setDicomInfoTemp.put(a.getFloatValues()[0], list);
					
					
					// Pegando o BufferedImage para montar a IJ Stack
					SourceImage sImg = new SourceImage(list);	
					
					// Adicionando no Set - O indexador é o SliceLocation				
					set.put(a.getFloatValues()[0], sImg.getBufferedImage());	
					
					//System.out.println("max: "+  sImg.getMaximum() + " min: "+sImg.getMinimum());
					if(a.getFloatValues()[0] - 69.1 < 0){
						int[] storedPixelValues  = sImg.getBufferedImage().getSampleModel().getPixel(372,91,(int[])null,sImg.getBufferedImage().getRaster().getDataBuffer());
						System.out.println(a.getFloatValues()[0]+ "(): "+storedPixelValues[0]);
						
					}
					
				}
				
				
				//(372,91): 2618
				//int[] storedPixelValues  = getSlice().getBufferedImage().getSampleModel().getPixel(e.getX(),e.getY(),(int[])null,getSlice().getBufferedImage().getRaster().getDataBuffer());
				//System.out.println("("+e.getX()+","+e.getY()+"): "+storedPixelValues[0]);
				
				// Depois de ler todas imagens - montar o stack						
				ImageStack istack = new ImageStack(width, height);
				count=0;
				for(Float key : set.keySet()){	
					
					//dicom info set
					setDicomInfo.put(count, setDicomInfoTemp.get(key));
					//indexes set
					setIndex.put(count, key);
					
					count++;
								
					try{
						ImagePlus ip = new ImagePlus(key+"", set.get(key));	
						istack.addSlice(key+"", ip.getProcessor());		
					}catch(Exception e){
						e.printStackTrace();
					}
				}			
				ImagePlus ip = new ImagePlus("Slices DICOM", istack);
	
				
				ci.set = set;			
				ci.index = setIndex;
				ci.imagePlus = ip;
				ci.dicom = dicomInfo;
				ci.setDicomInfo = setDicomInfo;
	
				Main.log("Done!");
				ip.show();
				ip.getWindow().setLocation(40, 555);
				
				return ci;		
			}
		}
		
		return null;
	}
}
