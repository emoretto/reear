package br.eng.moretto.earmaker.dicom;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.awt.image.BufferedImageOp;

import com.pixelmed.dicom.AttributeList;
import com.pixelmed.dicom.DicomException;
import com.pixelmed.dicom.TagFromName;

import br.eng.moretto.earmaker.CurrentImage;

public class GantryTiltCorrection {

	/**
	 * Calculo de correcao em Y do Gantry/Detector Tilt (0018,1120) 
	 * 
	 * http://www.mathworks.com/matlabcentral/fileexchange/24458-dicom-gantry-tilt-correction/content/gantrycorrect.m
	 * 
	 * 
	 * @param input buff
	 * @param current slice
	 * @return
	 */
	public static BufferedImage transform(BufferedImage buff, CurrentImage ci){
		
//		offsetmatrix = [1 0 0;0 1 0;0 (Offset*-1) 1];
		
		AttributeList list = ci.setDicomInfo.get(ci.imagePlus.getSlice()-1);
		double gantryTilt = 0.0;
		double sliceThickness = 0.0;
		double sliceSpacing = 1.0;
		
		try {
			gantryTilt = list.get(TagFromName.GantryDetectorTilt).getFloatValues()[0];
			sliceThickness = list.get(TagFromName.SliceThickness).getFloatValues()[0];
			//sliceSpacing = list.get(TagFromName.SpacingBetweenSlices).getFloatValues()[0];
		} catch (DicomException e) {
			e.printStackTrace();
		}
		
		if(gantryTilt == 0.0)
			return buff;
		
		System.out.println("***** Gantry Detector Tilt detected:  " + gantryTilt);
		
		int originalHeight = buff.getHeight();
		
		double GNTtan = Math.tan( Math.toRadians(  gantryTilt ) );  		
		double layer = ci.imagePlus.getSlice();
		double offset = GNTtan * sliceThickness * (layer-1) / sliceThickness;		
		
		AffineTransform translate = AffineTransform.getTranslateInstance(1,offset*-1);
		//System.out.println(translate.toString());
		
		BufferedImageOp bio;
		bio = new AffineTransformOp(translate,AffineTransformOp.TYPE_BILINEAR);
		BufferedImage res = bio.filter(buff,null);
		

		
		/*
		 * Como a imagem eh deslocada, preenchemos o inicio (y) dela com a primeira linha da imagem
		 * */		
		Graphics2D g2d = (Graphics2D) buff.getGraphics();
		g2d.setColor(Color.white);
 		g2d.fillRect(0, 0, buff.getWidth(), buff.getHeight());
		
		int yOffset = res.getHeight() - originalHeight;
		
		for(int i=0 ; i < yOffset*2 ; i++){
			g2d.drawImage(res.getSubimage(0, yOffset, buff.getWidth(), 1), 0, i, null);
		}		
		
		g2d.drawImage(res, 0, yOffset, null);
		
		return buff;
}
}
