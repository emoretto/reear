package br.eng.moretto.earmaker.dicom;

import ij.CompositeImage;
import ij.ImagePlus;
import ij.io.OpenDialog;
import ij.measure.Calibration;
import ij.plugin.DICOM;
import ij.process.LUT;
import ij.util.DicomTools;

import java.io.IOException;

import br.eng.moretto.earmaker.CurrentImage;
import loci.formats.FormatException;
import loci.formats.IFormatReader;
import loci.plugins.BF;
import loci.plugins.in.ImporterOptions;

/**
 * @author emoretto
 * 
 *         hu = pixel_value * slope + intercept
 * 
 *         0028,1052 Rescale Intercept = -1024 
 *         0028,1053 Rescale Slope = 1
 * 
 *         url: https://www.idlcoyote.com/fileio_tips/hounsfield.html
 * 
 */
public class DicomOpener {

	public static CurrentImage run() throws FormatException, IOException {

		OpenDialog od = new OpenDialog("Open Image File...");

		ImporterOptions options = new ImporterOptions();
		options.setGroupFiles(true);
		options.setId(od.getPath());
		ImagePlus dicom = BF.openImagePlus(options)[0];
		//dicom.show();

		// Raw Meta-data
		System.out.println(dicom.getProperty("Info"));

		// Dicom Meta-data
		DICOM dicomInfo = null;
		Calibration cal = null;
		
		try{
			dicomInfo = new DICOM();
			dicomInfo.open(od.getPath());
	
			System.out.println(DicomTools.getTag(dicomInfo, "0028,0010"));
			System.out.println(DicomTools.getTag(dicomInfo, "0028,0011"));
			System.out.println(DicomTools.getTag(dicomInfo, "0018,0050"));
	
			cal = dicom.getCalibration();
			double physicalX = cal.pixelWidth;
			double physicalY = cal.pixelHeight;
			double physicalZ = cal.pixelHeight;
			String spaceUnit = cal.getUnit();
			double physicalT = cal.frameInterval;
			String timeUnit = cal.getTimeUnit();
	
			System.out.println(physicalY);
			System.out.println(physicalZ);
			
		}catch(Exception e){
			e.printStackTrace();
		}

		CurrentImage ci = new CurrentImage(dicom, dicomInfo, cal);
		
		return ci;
		
		/*
		 * DicomReader ir = new DicomReader(); // ImageReader ir = new
		 * ImageReader(); //ImageProcessorReader ir = new
		 * ImageProcessorReader(); ir.setGroupFiles(true);
		 * ir.setId(od.getPath());
		 * 
		 * System.out.println(ir.getImageCount());
		 * 
		 * //ImageProcessor ip = ir.openProcessors(0)[0];
		 * 
		 * System.out.println(ir.getSizeZ());
		 * 
		 * 
		 * System.out.println("aoaoaoaoaoao");
		 * 
		 * DicomReader dr = new DicomReader();
		 * 
		 * Calibration cal = thisDicom.getCalibration(); double physicalX =
		 * cal.pixelWidth; double physicalY = cal.pixelHeight; double physicalZ
		 * = cal.pixelHeight; String spaceUnit = cal.getUnit(); double physicalT
		 * = cal.frameInterval; String timeUnit = cal.getTimeUnit();
		 * 
		 * System.out.println(physicalY); System.out.println(physicalZ);
		 * 
		 * /* String id = dir + name;
		 * 
		 * ImageProcessorReader r = new ImageProcessorReader( new
		 * ChannelSeparator(LociPrefs.makeImageReader()));
		 * 
		 * try { IJ.showStatus("Examining file " + name); r.setId(id); int num =
		 * r.getImageCount(); int width = r.getSizeX(); int height =
		 * r.getSizeY(); ImageStack stack = new ImageStack(width, height);
		 * byte[][][] lookupTable = new byte[r.getSizeC()][][];
		 * 
		 * for (int i=0; i<num; i++) { IJ.showStatus("Reading image plane #" +
		 * (i + 1) + "/" + num); ImageProcessor ip = r.openProcessors(i)[0];
		 * stack.addSlice("" + (i + 1), ip); int channel = r.getZCTCoords(i)[1];
		 * lookupTable[channel] = r.get8BitLookupTable(); }
		 * IJ.showStatus("Constructing image"); ImagePlus imp = new
		 * ImagePlus(name, stack);
		 * 
		 * ImagePlus colorizedImage = DicomOpener.applyLookupTables(r, imp,
		 * lookupTable); r.close();
		 * 
		 * colorizedImage.show(); IJ.showStatus(""); } catch (FormatException
		 * exc) { IJ.error("Sorry, an error occurred: " + exc.getMessage()); }
		 * catch (IOException exc) { IJ.error("Sorry, an error occurred: " +
		 * exc.getMessage()); }
		 */
	}

	private static ImagePlus applyLookupTables(IFormatReader r, ImagePlus imp,
			byte[][][] lookupTable) {
		// apply color lookup tables, if present
		// this requires ImageJ v1.39 or higher
		if (r.isIndexed()) {
			CompositeImage composite = new CompositeImage(imp,
					CompositeImage.COLOR);
			for (int c = 0; c < r.getSizeC(); c++) {
				composite.setPosition(c + 1, 1, 1);
				LUT lut = new LUT(lookupTable[c][0], lookupTable[c][1],
						lookupTable[c][2]);
				composite.setChannelLut(lut);
			}
			composite.setPosition(1, 1, 1);
			return composite;
		}
		return imp;
	}
}
