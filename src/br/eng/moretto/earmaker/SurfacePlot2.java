package br.eng.moretto.earmaker;

import ij.ImagePlus;
import ij.process.StackConverter;
import ij3d.Content;
import ij3d.Image3DUniverse;

public class SurfacePlot2  extends Thread implements Runnable {

	ImagePlus imp = null;
	public SurfacePlot2(ImagePlus imp) {
		this.imp = imp;
	}
	
	@SuppressWarnings("deprecation")
	@Override
	public void run() {
		
		System.out.println("Started");
		

	    // Open an image
	    new StackConverter(imp).convertToGray8();

	    // Create a universe and show it
	    Image3DUniverse univ = new Image3DUniverse();
	    univ.show();

	    // Add the image as a volume rendering
	    Content c = univ.addVoltex(imp);
	    
	    int res = Content.getDefaultResamplingFactor(imp, Content.VOLUME);
		int thr = Content.getDefaultThreshold(imp, Content.VOLUME);
		
		int resampling = 1;
		
		 // Add an isosurface
	    c = univ.addMesh(imp);
	    
	    
	    //sleep(5);
	    
//	    univ.addContent(imp, null, imp.getTitle(), thr, new boolean[] {true, true, true}, resampling, Content.VOLUME);
	    
	   // sleep(5);

	    /*
	    // Display the image as orthslices
	    c.displayAs(Content.ORTHO);
	    sleep(5);

	    // Remove the content
	    univ.removeContent(c.getName());
	    sleep(5);

	   

	    // display slice 10 as a surface plot
	    univ.removeContent(c.getName());
	    imp.setSlice(10);
	    c = univ.addSurfacePlot(imp);
	    sleep(5);

	    // remove all contents
	    univ.removeAllContents();

	    // close
	    univ.close();
	    */
		
	}

	private static void sleep(int sec) {
		try {
			Thread.sleep(sec * 1000);
		} catch(InterruptedException e) {
			System.out.println(e.getMessage());
		}
	}
	
	
}
