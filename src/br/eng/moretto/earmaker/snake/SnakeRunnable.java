package br.eng.moretto.earmaker.snake;

import java.awt.Point;
import java.awt.image.BufferedImage;

public class SnakeRunnable {
	
	private BufferedImage image = null;
	
	// the true snake object
	private Snake snakeinstance = null;
	

	private int[][] chanel_gradient = null;
	private int[][] chanel_flow = null;
	
	
	public SnakeRunnable(BufferedImage image) {
		this.image = image;
	}
	
	// Snake Runnable
	final Runnable snakerunner = new Runnable() {
		public void run() {
			try {
				startsnake();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
	};
	
	
	// ---------------------------------------------------------------------
	//                         START SNAKE SEGMENTATION
	// ---------------------------------------------------------------------

	private void startsnake() {
		
		int W = image.getWidth();
		int H = image.getHeight();

		// initial points
		double radius = (W/2 + H/2) / 2;
		double perimeter = 6.28 * radius;
		int nmb = (int) (perimeter / 16);
		Point[] circle = new Point[nmb];
		for (int i = 0; i < circle.length; i++) {
			double x = (W / 2 + 0) + (W / 2 - 2) * Math.cos((6.28 * i) / circle.length);
			double y = (H / 2 + 0) + (H / 2 - 2) * Math.sin((6.28 * i) / circle.length);
			circle[i] = new Point((int) x, (int) y);
		}

		// create snake instance
		//snakeinstance = new Snake(image, W, H, circle);

		
		// snake base parameters
//		snakeinstance.alpha = Double.parseDouble(txtAlpha.getText());
//		snakeinstance.beta = Double.parseDouble(txtBeta.getText());
//		snakeinstance.gamma = Double.parseDouble(txtGamma.getText());
//		snakeinstance.delta = Double.parseDouble(txtDelta.getText());

		// snake extra parameters
//		snakeinstance.SNAKEGUI = this;
//		snakeinstance.SHOWANIMATION = cbShowAnim.isSelected();
//		snakeinstance.AUTOADAPT = cbAutoadapt.isSelected();
//		snakeinstance.AUTOADAPT_LOOP = Integer.parseInt(txtStep.getText());
//		snakeinstance.AUTOADAPT_MINLEN = Integer.parseInt(txtMinlen.getText());
//		snakeinstance.AUTOADAPT_MAXLEN = Integer.parseInt(txtMaxlen.getText());
//		snakeinstance.MAXITERATION = Integer.parseInt(txtMaxiter.getText());

		// animate snake
		System.out.println("initial snake points:" + snakeinstance.snake.size());
		int nmbloop = snakeinstance.loop();
		System.out.println("final snake points:" + snakeinstance.snake.size());
		System.out.println("iterations: " + nmbloop);

		// display final result
		//display();

		System.out.println("END - Snake points: "+snakeinstance.snake.size());
	}
	
}
