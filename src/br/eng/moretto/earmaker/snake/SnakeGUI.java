package br.eng.moretto.earmaker.snake;
import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Polygon;
import java.awt.color.ColorSpace;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSlider;
import javax.swing.JTextField;
import javax.swing.WindowConstants;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import br.eng.moretto.earmaker.Main;

public class SnakeGUI {

	// --- MODEL -----------------------------------------------------------

	// the true snake object
	private Snake snakeinstance = null;

	// ---- IMAGE DATA -----------------------------------------------------

	private BufferedImage image = null;
	private BufferedImage imageanimation = null;
	private int[][] chanel_gradient = null;
	private int[][] chanel_flow = null;

	// --- SWING COMPONENTS ------------------------------------------------

	private JLabel label0 = new JLabel("");
	private JLabel label1 = new JLabel("");
	private JCheckBox cbShowAnim = new JCheckBox("Show animation");
	private JTextField txtMaxiter = new JTextField("900", 3);

	private JSlider slideThreshold = new JSlider(1, 100, 50);
	private JTextField txtAlpha = new JTextField("1.0", 3);
	private JTextField txtBeta = new JTextField("1.0", 3);
	private JTextField txtGamma = new JTextField("1.0", 3);
	private JTextField txtDelta = new JTextField("1.0", 3);

	private JCheckBox cbAutoadapt = new JCheckBox("Auto-Adapt");
	private JTextField txtStep = new JTextField("10", 3);
	private JTextField txtMinlen = new JTextField("1", 1);
	private JTextField txtMaxlen = new JTextField("5", 5);

	
	// Snake Runnable
	final Runnable snakerunner = new Runnable() {
		public void run() {
			try {
				startsnake();
			} catch (Exception ex) {
				error(ex.getMessage(), ex);
			}
		}
	};
	
	
	final JFrame frame = new JFrame();//"Contorno Ativo - Snake Algorithm");
	final JDialog dialog = new JDialog();
	
	// --- Create the GUI ---------------------------------------------------

	public SnakeGUI(final Main main) {
		label0.setVerticalTextPosition(JLabel.BOTTOM);
		label0.setHorizontalTextPosition(JLabel.CENTER);
		label1.setVerticalTextPosition(JLabel.BOTTOM);
		label1.setHorizontalTextPosition(JLabel.CENTER);
		
		frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

		final JPanel panneau = new JPanel();
		panneau.add(label0);
		panneau.add(label1);
		final JScrollPane scrollPane = new JScrollPane(panneau);

//		JButton buttonLoad = new JButton("Load image");
		JButton buttonRun = new JButton("Reload");
		
		JButton buttonDone = new JButton("Ok");

		final JPanel buttonPanel = new JPanel();
		//buttonPanel.add(buttonLoad);
		//buttonPanel.add(buttonRun);
		//buttonPanel.add(cbShowAnim);
		
		cbShowAnim.setSelected(true);

		final JPanel coefPanel = new JPanel();
		coefPanel.add(buttonRun);
		//coefPanel.add(new JLabel("Max. Iteration:"));
		//coefPanel.add(txtMaxiter);
		
		coefPanel.add(new JLabel("gradient thld:"));
		coefPanel.add(slideThreshold);
		coefPanel.add(new JLabel("alpha:"));
		coefPanel.add(txtAlpha);
		coefPanel.add(new JLabel("beta:"));
		coefPanel.add(txtBeta);
		coefPanel.add(new JLabel("gamma:"));
		coefPanel.add(txtGamma);
		coefPanel.add(new JLabel("delta:"));
		coefPanel.add(txtDelta);

		final JPanel adpatPanel = new JPanel();
		//adpatPanel.add(cbAutoadapt);
		//adpatPanel.add(new JLabel("every X iterations:"));
		//adpatPanel.add(txtStep);
		adpatPanel.add(new JLabel("segmento min:"));
		adpatPanel.add(txtMinlen);
		adpatPanel.add(new JLabel("segmento max:"));
		adpatPanel.add(txtMaxlen);
		adpatPanel.add(buttonDone);
		cbAutoadapt.setSelected(true);

		final JPanel mainPanel = new JPanel();
		mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.Y_AXIS));
		mainPanel.add(buttonPanel);
		mainPanel.add(coefPanel);
		mainPanel.add(adpatPanel);

		// frame
		frame.getContentPane().add(scrollPane, BorderLayout.CENTER);
		frame.getContentPane().add(mainPanel, BorderLayout.PAGE_END);
		frame.setSize(600, 400);
		//dialog.setSize(600, 400);
		//dialog.add(frame);
//		frame.setModal(true);
		frame.setVisible(true);
		//dialog.setVisible(true);
		

		/* DONE - gerando a imagem e retornando para o Main */
		buttonDone.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				BufferedImage img = new BufferedImage(image.getWidth(), image.getHeight(), BufferedImage.TYPE_INT_RGB);
				Graphics2D gc = (Graphics2D) img.getGraphics();

				//cores invertidas
				// bg
				gc.setBackground( Color.BLACK );
				gc.setColor( Color.BLACK );
				gc.fillRect(0, 0, image.getWidth(), image.getHeight());
				
				// draw snake lines
				gc.setColor( Color.WHITE );
				gc.setStroke(new BasicStroke(2.0f));
				List<Point> snakepoints = snakeinstance.snake;
				
				Polygon polygon = new Polygon();
				
				for (int i = 0; i < snakepoints.size(); i++) {
					int j = (i + 1) % snakepoints.size();
					Point p1 = snakepoints.get(i);
					Point p2 = snakepoints.get(j);
					polygon.addPoint(p1.x, p1.y);

					//borda(?)
					gc.drawLine(p1.x, p1.y, p2.x, p2.y);
				}
				gc.fillPolygon(polygon);
				
				main.finish(img);
				//frame.dispose();
			}
		});
		
		// ActionListener "LOAD"
		/*buttonLoad.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				FileDialog filedialog = new FileDialog(frame, "Choose an image file");
				filedialog.setVisible(true);
				String filename = filedialog.getFile();
				String directory = filedialog.getDirectory();
				if (filename == null)
					return;
				File file = new File(directory + File.separator + filename);
				mainPanel.setVisible(false);
				try {
					loadimage(file);
					computegflow();
				} catch (Exception ex) {
					error(ex.getMessage(), ex);
				}
				mainPanel.setVisible(true);
			}
		});
		*/

		// ActionListener "SLIDE"
		slideThreshold.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				if (slideThreshold.getValueIsAdjusting())
					return;
				mainPanel.setVisible(false);
				try {
					computegflow();
				} catch (Exception ex) {
					error(ex.getMessage(), ex);
				}
				mainPanel.setVisible(true);
			}
		});

		

		// ActionListener "RUN"
		buttonRun.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new Thread(snakerunner).start();
			}
		});

		/*
		try {
			mainPanel.setVisible(false);
			loadimage(new File("trefle.png"));
			computegflow();
			mainPanel.setVisible(true);
		} catch (Exception ex) {
			error(ex.getMessage(), ex);
		}*/
		
		
		
	}

	// error (exception) display
	private static void error(String text, Exception ex) {
		if (ex != null) {
			StringWriter sw = new StringWriter();
			PrintWriter pw = new PrintWriter(sw);
			ex.printStackTrace(pw);
			String s = sw.toString();
			s = s.substring(0, Math.min(512, s.length()));
			text = text + "\n\n" + s + " (...)";
		}
		JOptionPane.showMessageDialog(null, text, "Error", JOptionPane.ERROR_MESSAGE);
	}
	
	// ---------------------------------------------------------------------
	//                        DRAWING PRIMITIVES
	// ---------------------------------------------------------------------

	public void display() { /* callback from snakeinstance */
		Graphics2D gc = imageanimation.createGraphics();
		
		// draw background image
        gc.drawImage(image,0,0,null);

		// draw snake lines
		gc.setColor( Color.RED );
		gc.setStroke(new BasicStroke(2.0f));
		List<Point> snakepoints = snakeinstance.snake;
		for (int i = 0; i < snakepoints.size(); i++) {
			int j = (i + 1) % snakepoints.size();
			Point p1 = snakepoints.get(i);
			Point p2 = snakepoints.get(j);
			gc.drawLine(p1.x, p1.y, p2.x, p2.y);
		}

		// draw snake points
		gc.setColor( Color.ORANGE );
		for (int i = 0; i < snakepoints.size(); i++) {
			Point p = snakepoints.get(i);
			gc.fillRect(p.x-2, p.y-2, 5, 5);
		}

		try {
			Thread.sleep(3);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		// swing display
		label1.setIcon(new ImageIcon(imageanimation));
	}

	// ---------------------------------------------------------------------
	//                       IMAGE LOADING/COMPUTATION
	// ---------------------------------------------------------------------

	public void loadimage(BufferedImage buff) throws IOException {
			
		this.frame.setSize(this.frame.getWidth(), buff.getHeight()+130);
		
		image = null;
		image = buff;
		imageanimation = new BufferedImage(image.getWidth(),image.getHeight(),ColorSpace.TYPE_RGB);

		computegflow();
		
		
		// swing display
		label1.setIcon(new ImageIcon(image));
		
		new Thread(snakerunner).start();
	}

	private void computegflow() {
		int W = image.getWidth();
		int H = image.getHeight();

		int THRESHOLD = slideThreshold.getValue();

		// GrayLevelScale (Luminance)
		int[][] clum = new int[W][H];
		for (int y = 0; y < H; y++)
			for (int x = 0; x < W; x++) {
				int rgb=image.getRGB(x,y);
				int r = (rgb >>16 ) & 0xFF;
				int g = (rgb >> 8 ) & 0xFF;
				int b = rgb & 0xFF;
				clum[x][y] = (int)(0.299*r + 0.587*g + 0.114*b);  
			}
				
		// Gradient (sobel)
		this.chanel_gradient = new int[W][H]; 
		int maxgradient=0;
		for (int y = 0; y < H-2; y++)
			for (int x = 0; x < W-2; x++) {
				int p00 = clum[x+0][y+0]; int p10 = clum[x+1][y+0]; int p20 = clum[x+2][y+0];
				int p01 = clum[x+0][y+1]; /*-------------------- */ int p21 = clum[x+2][y+1];
				int p02 = clum[x+0][y+2]; int p12 = clum[x+1][y+2]; int p22 = clum[x+2][y+2];
				int sx = (p20+2*p21+p22)-(p00+2*p01+p02);
				int sy = (p02+2*p12+p22)-(p00+2*p10+p10);
				int snorm = (int)Math.sqrt(sx*sx+sy*sy);
				chanel_gradient[x+1][y+1]=snorm;
				maxgradient=Math.max(maxgradient, snorm);
			}
		
		

		
		
		// thresholding
		boolean[][] binarygradient = new boolean[W][H];
		for (int y = 0; y < H; y++)
			for (int x = 0; x < W; x++)
				if (chanel_gradient[x][y] > THRESHOLD*maxgradient/100) {
					binarygradient[x][y]=true;
				} else {
					chanel_gradient[x][y]=0;
				}

		// distance map to binarized gradient
		chanel_flow = new int[W][H];
		double[][] cdist = new ChamferDistance(ChamferDistance.chamfer5).compute(binarygradient, W,H);
		for (int y = 0; y < H; y++)
			for (int x = 0; x < W; x++)
				chanel_flow[x][y]=(int)(5*cdist[x][y]);
		
		
		
		
		// show flow + gradient
		int[] rgb = new int[3];
		BufferedImage imgflow = new BufferedImage(W, H, ColorSpace.TYPE_RGB);
		for (int y = 0; y < H; y++) {
			for (int x = 0; x < W; x++) {
				int vflow = chanel_flow[x][y];
				int vgrad = binarygradient[x][y]?255:0;

				if (vgrad > 0) {
					rgb[0] = 0;
					rgb[1] = vgrad;
					rgb[2] = 0;
				} else {
					rgb[0] = Math.max(0, 255 - vflow);
					rgb[1] = 0;
					rgb[2] = 0;
				}
				int irgb = (0xFF<<24)+(rgb[0]<<16)+(rgb[1]<<8)+rgb[2];
				imgflow.setRGB(x, y, irgb);
			}
		}

		// swing display
		label0.setIcon(new ImageIcon(imgflow));
	}

	// ---------------------------------------------------------------------
	//                         START SNAKE SEGMENTATION
	// ---------------------------------------------------------------------

	private void startsnake() {
		int W = image.getWidth();
		int H = image.getHeight();
		int MAXLEN = Integer.parseInt(txtMaxlen.getText()); /* max segment length */

		// initial points
		double radius = (W/2 + H/2) / 2;
		double perimeter = 6.28 * radius;
		int nmb = (int) (perimeter / MAXLEN);
		Point[] circle = new Point[nmb];
		for (int i = 0; i < circle.length; i++) {
			double x = (W / 2 + 0) + (W / 2 - 2) * Math.cos((6.28 * i) / circle.length);
			double y = (H / 2 + 0) + (H / 2 - 2) * Math.sin((6.28 * i) / circle.length);
			circle[i] = new Point((int) x, (int) y);
		}

		// create snake instance
		snakeinstance = new Snake(W, H, chanel_gradient, chanel_flow, circle);

		// snake base parameters
		snakeinstance.alpha = Double.parseDouble(txtAlpha.getText());
		snakeinstance.beta = Double.parseDouble(txtBeta.getText());
		snakeinstance.gamma = Double.parseDouble(txtGamma.getText());
		snakeinstance.delta = Double.parseDouble(txtDelta.getText());

		// snake extra parameters
		snakeinstance.SNAKEGUI = this;
		snakeinstance.SHOWANIMATION = cbShowAnim.isSelected();
		snakeinstance.AUTOADAPT = cbAutoadapt.isSelected();
		snakeinstance.AUTOADAPT_LOOP = Integer.parseInt(txtStep.getText());
		snakeinstance.AUTOADAPT_MINLEN = Integer.parseInt(txtMinlen.getText());
		snakeinstance.AUTOADAPT_MAXLEN = Integer.parseInt(txtMaxlen.getText());
		snakeinstance.MAXITERATION = Integer.parseInt(txtMaxiter.getText());

		// animate snake
		System.out.println("initial snake points:" + snakeinstance.snake.size());
		int nmbloop = snakeinstance.loop();
		System.out.println("final snake points:" + snakeinstance.snake.size());
		System.out.println("iterations: " + nmbloop);

		// display final result
		display();

		System.out.println("END - Snake points: "+snakeinstance.snake.size());
	}

	// ---------------------------------------------------------------------

	public static void main(String[] args) {
		Thread.currentThread().setPriority(Thread.MIN_PRIORITY);
		new SnakeGUI(null);
	}
}
