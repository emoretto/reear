package br.eng.moretto.earmaker.snake;

import ij.ImagePlus;

import java.awt.color.ColorSpace;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class SnakeGVF {

	public static void main(String[] args) throws IOException {
		
		BufferedImage image = ImageIO.read(new File("resources/U64.jpg"));
		
		int W = image.getWidth();
		int H = image.getHeight();
		
		
		// GrayLevelScale (Luminance)
		int[][] clum = new int[W][H];
		for (int y = 0; y < H; y++)
			for (int x = 0; x < W; x++) {
				int rgb=image.getRGB(x,y);
				int r = (rgb >>16 ) & 0xFF;
				int g = (rgb >> 8 ) & 0xFF;
				int b = rgb & 0xFF;
				clum[x][y] = (int)(0.299*r + 0.587*g + 0.114*b);  
			}
		
		showImage(clum, W, H);
		
		
		
		// Gradient (sobel)
		int[][] chanel_gradient = null;
		int[][] chanel_flow = null;
		chanel_gradient = new int[W][H]; 
		int maxgradient=0;
		for (int y = 0; y < H-2; y++)
			for (int x = 0; x < W-2; x++) {
				int p00 = clum[x+0][y+0]; int p10 = clum[x+1][y+0]; int p20 = clum[x+2][y+0];
				int p01 = clum[x+0][y+1]; /*-------------------- */ int p21 = clum[x+2][y+1];
				int p02 = clum[x+0][y+2]; int p12 = clum[x+1][y+2]; int p22 = clum[x+2][y+2];
				int sx = (p20+2*p21+p22)-(p00+2*p01+p02);
				int sy = (p02+2*p12+p22)-(p00+2*p10+p10);
				int snorm = (int)Math.sqrt(sx*sx+sy*sy);
				chanel_gradient[x+1][y+1]=snorm;
				maxgradient=Math.max(maxgradient, snorm);
			}
		showImage(chanel_gradient, W, H);
				
		
		// thresholding
		boolean[][] binarygradient = new boolean[W][H];
		for (int y = 0; y < H; y++)
			for (int x = 0; x < W; x++)
				if (chanel_gradient[x][y] > 80*maxgradient/100) {
					binarygradient[x][y]=true;
				} else {
					chanel_gradient[x][y]=0;
				}
			
		showImage(chanel_gradient, W, H);
	}
	
	
	static void showImage(int[][] img, int W, int H){

		// show flow + gradient
		int[] rgb = new int[3];
		BufferedImage imgflow = new BufferedImage(W, H, ColorSpace.TYPE_GRAY);
		for (int y = 0; y < H; y++) {
			for (int x = 0; x < W; x++) {
				
				int vgrad = img[x][y];
				
				rgb[0] = vgrad;
				rgb[1] = vgrad;
				rgb[2] = vgrad;
			
				
				int irgb = (0xFF<<24)+(rgb[0]<<16)+(rgb[1]<<8)+rgb[2];
				imgflow.setRGB(x, y, irgb);
			}
		}
		
		ImagePlus ip = new ImagePlus("Img", imgflow);
		ip.show();
		
	}
}

