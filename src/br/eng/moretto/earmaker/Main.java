package br.eng.moretto.earmaker;

import ij.IJ;
import ij.ImagePlus;
import ij.ImageStack;
import ij.WindowManager;
import ij.gui.Roi;
import ij.measure.Calibration;
import ij.process.StackConverter;
import ij.util.DicomTools;
import ij3d.ImageJ3DViewer;

import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.RescaleOp;
import java.awt.image.WritableRaster;
import java.io.File;
import java.io.IOException;
import java.util.TreeMap;

import javax.imageio.ImageIO;
import javax.swing.JOptionPane;

import br.eng.moretto.earmaker.dicom.GantryTiltCorrection;
import br.eng.moretto.earmaker.dicom.PixelMed;
import br.eng.moretto.earmaker.gl3d.Object3D;
import br.eng.moretto.earmaker.gl3d.Viewer3D;
import br.eng.moretto.earmaker.regiongrow.GrowProbResult;
import br.eng.moretto.earmaker.regiongrow.QueueGrowMold;
import br.eng.moretto.earmaker.regiongrow.QueueGrowProb;
import br.eng.moretto.earmaker.snake.SnakeGUI;


/**
 * ------->>>>>>  RODAR COM vm arguments: <<<<<< -----------
 * 
 * -Xmx4G -Djava.ext.dirs=''
 * 
 * O Java 3D (jogl) agora vem com as bibliotecas nativas embutidas nos jars. 
 * Para outra plataforma, basta trocar o library path do eclipse para as bibliotecas específicas.
 * O parametro anterior da Vm serve para nao utilizar as bibliotecas externas nativas
 * 
 * 
 * Notes:
 * 
 * - JPEG-less: nao abre, é preciso abrir no osirix e exportar para DICOM JPEG
 * - Na exportacao do Osirix, DICOM SEM COMPRESSAO abre com erro aqui, fica tudo invertido
 * 
 * 
 * - Snake Integrado OK e testado. Porém utilizei o botao do 3D pq o netbeans ta zuado.
 * 
 * - O Snake tá "engrossando" o modelo.
 *  
 * 
 * - Gantry Tilt feito!! Parece ok, precisava de avaliar melhor, parametrizar e calcular o esquema da distancia entre camadas
 * 1.) It is common for CT scans to have variable slice thicknesses - bear this in mind when viewing your images (many viewers and formats such as NIfTI require equidistant slices) 
 2.) In my experience with CT scans you can get the slice thickness by checking 0018,0050. This is different from MRI, where some vendors do not handle this correctly when their is a slice gap. 
 3.) You can also get the slice thickness by using the Image Position Patient (0020,0032) of sequential slices - which is the best thing to do for MRI. This gives you the X,Y,Z dimensions for the two slices, so the distance is computed with pythagorean theorem [dx = sqrt( (x1-x2)^2 + (y1-y2)^2 + (z1-z2)^2) ]. The trick here is that for CT scans this refers to the distance on the table, and when their is gantry tilt the distance between actual slices is different - you can solve this with trigonometry. For example if the gantry tilt is 18.2-degrees you will want to compute scale=cos(radians(18.2)), which would give you 0.95. So if your dx between slices on the table is 5.263mm and the gantry tilt is 18.2degrees, the true distance between slices is 5mm. This final value SHOULD match what you got from 0018,0050. 
 By the way, the latest versions of my dcm2nii should handle both gantry tilt and variable slice distances.  
 * 
 * TODO
 * 
 * - QueueGrowMold NÃO FICOU BAO... DEU RUIM... TA ENTRANDO DENTRO DA IMG.. PENSAR TUDO DE NOVO
 * 
 * 
 * - Parametrizar na tela os parametros do classificador probabilístico
 * 
 */

public class Main {
	
	static CurrentImage ci = null;
	
	protected Integer startSlice = -1;
	protected Integer endSlice = -1;
	protected Integer currentSlice = 0;
	protected Rectangle roi;
	protected static UI ui;
	protected SnakeGUI snakeGUI;
	
	double centerX = 0;
	
	ImagePlus debug;
	
	boolean subtractiveMode = false;
        boolean brush = false;
	
	public int threshold = 400;
	public int lowerThreshold = -200;
	public int differenceThreshold = 400;
	
	ImagePlus slice;
	ImagePlus sliceCrop;
	ImagePlus sliceCropMirror;
	ImagePlus resultStack;
	ImagePlus mirror;
	
	TreeMap<Integer, ImagePlus> resultTree = new TreeMap<Integer, ImagePlus>();
	
	Viewer3D viewer3d;
	Object3D object3d;
	
	public static void main(String[] args) { new Main(); }
	
	public static void log(String str){ ui.log(str); }
	
	public static void console(String str){ if(str != null) ui.console(str); }
	
	protected BufferedImage getSlice(){
		return ci.set.get(ci.index.get( ci.imagePlus.getSlice()-1 ));
	}
	
	protected void setSlice(BufferedImage buff){
		 ci.set.put( ci.index.get( ci.imagePlus.getSlice()-1 ), buff);
	}
	
	public Main() {
		ui = new UI(this);
		ui.setVisible(true);
		
		log("Instruções:");
		log("1 - Selecionar uma área desejada sobre a orelha saudável");
		log("2 - Andar por todas as camadas pra ver se a área selecionada compreende todas as partes");
		log("3 - Voltar na primeira camada desejada e apertar em Processar Camada");
		log("4 - Ajustar o que for preciso e Salvar Camada");
		
		//Opening DICOM
		try{
			ci = PixelMed.run();
		}catch(Exception e){ JOptionPane.showMessageDialog(null, "Não foi possível abrir a imagem"); e.printStackTrace(); return; }
		
		
		//Show
		ci.imagePlus.show();
		
		ci.imagePlus.getWindow().getCanvas().addMouseListener(new MouseListener() {
			@Override public void mouseReleased(MouseEvent e) {	
				//updateMirrorRoi();				
			}
			@Override public void mousePressed(MouseEvent e) {		}
			@Override public void mouseExited(MouseEvent e) {		}
			@Override public void mouseEntered(MouseEvent e) {		}
			@Override public void mouseClicked(MouseEvent e) {
			
				BufferedImage src = getSlice();
				int[] storedPixelValues = src.getRaster().getPixel(e.getX(),e.getY(), new int[3]);
				
				log("("+e.getX()+","+e.getY()+"): "+(storedPixelValues[0] * ci.getRescaleSlope() + ci.getRescaleIntercept()));
				System.out.println("("+e.getX()+","+e.getY()+"): "+ (storedPixelValues[0] * ci.getRescaleSlope() + ci.getRescaleIntercept()));
								
				centerX = e.getX();
				
			}
		});
	}
	
	private void updateMirrorRoi(){
	
		//Propagando o ROI para a imagem mirror
		if(ci.imagePlus.getRoi() != null && mirror != null){
			
			Roi roi =  ci.imagePlus.getRoi();
						
			Roi newRoi = new Roi(roi.getBounds());
			
			double offset = roi.getBounds().getMinX() - centerX;
					
			
			double newX = roi.getBounds().getMinX() - (offset*2f) - roi.getBounds().getWidth();

			
			newRoi.setLocation((int) newX,roi.getBounds().y);
			
			
			mirror.setRoi(newRoi,true);
		}
	}
		
	public void setFirstLayer() {
	
		startSlice = ci.imagePlus.getSlice();
		log("Slice Inicial: "+ci.imagePlus.getSlice());
		currentSlice = startSlice;
	}
	
	BufferedImage biCrop, biCropMirror;
	
	
	// Processar Camada
	public void setLastLayer() {
	
		endSlice = ci.imagePlus.getSlice();
		log("Slice Final: "+ci.imagePlus.getSlice());
		
		System.out.println("Caixa de seleção: "+
				ci.imagePlus.getRoi().getBounds().getX()
				+","+ci.imagePlus.getRoi().getBounds().getY()
				+","+ci.imagePlus.getRoi().getBounds().getWidth()
				+","+ci.imagePlus.getRoi().getBounds().getHeight());

		roi = ci.imagePlus.getRoi().getBounds();
		
		
		// Crop		
		BufferedImage bi = getSlice();
		biCrop = bi.getSubimage(
				(int) ci.imagePlus.getRoi().getBounds().getX(),
				(int) ci.imagePlus.getRoi().getBounds().getY(),
				(int) ci.imagePlus.getRoi().getBounds().getWidth(),
				(int) ci.imagePlus.getRoi().getBounds().getHeight());
		
		sliceCrop = new ImagePlus("Crop", biCrop);
		
		
		
		if(subtractiveMode){
			// Mirror creation
			ImagePlus biMirror = new ImagePlus("temp", bi.getSubimage(0, 0, bi.getWidth(), bi.getHeight()));
			//IJ.run(biMirror, "Flip Horizontally", "");
			
			if(mirror == null) 
				mirror = new ImagePlus("Mirror", biMirror.getImage());
			else 
				mirror.setImage(biMirror.getImage());
			
			
			mirror.show();
			mirror.getWindow().setLocation(600, 555);
			
			updateMirrorRoi();	
			
			// Crop - Mirror		
			biCropMirror = bi.getSubimage(
					(int) mirror.getRoi().getBounds().getX(),
					(int) mirror.getRoi().getBounds().getY(),
					(int) mirror.getRoi().getBounds().getWidth(),
					(int) mirror.getRoi().getBounds().getHeight());
			
			sliceCropMirror = new ImagePlus("Crop", biCropMirror);			
		}
			
		
		//Threshold
		setThreshold(threshold);	
	}
	
	ImagePlus debugImg;
	
	public void setThreshold(int value) {
		
		threshold = value-1024;
		
		log("Threshold: "+value);
		
		//QueueGrowDICOM qg = new QueueGrowDICOM( biCrop, threshold );					
		//BufferedImage grow = qg.grow();
						
		QueueGrowProb qgp = new QueueGrowProb(biCrop, ci, threshold, lowerThreshold, differenceThreshold);
		GrowProbResult grow = qgp.grow();
	
		QueueGrowMold qgpMold = new QueueGrowMold(grow.result, ci, 1);
		GrowProbResult qrowMold = qgpMold.grow();
		
		//Com Gantry Correction!!
		BufferedImage gantry = GantryTiltCorrection.transform(qrowMold.result, ci);
		
		sliceCrop.setImage(gantry);
		scale(sliceCrop,false);
		ui.setImage(sliceCrop.getBufferedImage());
		
		
		
		if(subtractiveMode){
			//MIRROR
			QueueGrowProb qgp2 = new QueueGrowProb(biCropMirror, ci, threshold, lowerThreshold, differenceThreshold);
			GrowProbResult grow2 = qgp2.grow();
		
			QueueGrowMold qgpMold2 = new QueueGrowMold(grow2.result, ci, 1);
			GrowProbResult qrowMold2 = qgpMold2.grow();
			
			//Com Gantry Correction!!
			BufferedImage gantry2 = GantryTiltCorrection.transform(qrowMold2.result, ci);
			
			sliceCropMirror.setImage(gantry2);
			scale(sliceCropMirror,false);
			ui.setMirrorImage(sliceCropMirror.getBufferedImage());
		
		
			//Subtract - Result Image - para exibicao de como as imagens estao sendo subtraidas
			IJ.run(sliceCrop, "Flip Horizontally", "");				
			BufferedImage result = sliceCrop.getBufferedImage();
			BufferedImage mirrored = sliceCropMirror.getBufferedImage();
			
			for( int i = 0 ; i < result.getWidth() ; i++){
				for( int j = 0 ; j < result.getHeight() ; j++){
					  					
					if(mirrored.getRGB(i, j) != -1)
						result.setRGB(i, j, 0xffFF0000);
				}
			}		
			ui.setResultImage(result);	
			
		
			BufferedImage finalImage = sliceCrop.getBufferedImage();
			
			for( int i = 0 ; i < result.getWidth() ; i++){
				for( int j = 0 ; j < result.getHeight() ; j++){
					  					
					if(result.getRGB(i, j) == 0xff000000)
						finalImage.setRGB(i, j, 0xffFFFFFF);
					else
						finalImage.setRGB(i, j, 0xff000000);
						
				}
			}	
		
			ui.setFinalImage(finalImage);	

			//Imagem de onde iremos salvar as fatias do molde
			sliceCrop.setImage(finalImage);
			
			
		}else{
			//Imagem de onde iremos salvar as fatias do molde
			//sliceCrop.setImage(sliceCropMirror.getBufferedImage());		
			
			BufferedImage finalImage = sliceCrop.getBufferedImage();
			
			for( int i = 0 ; i < finalImage.getWidth() ; i++){
				for( int j = 0 ; j < finalImage.getHeight() ; j++){
					  					
					if(finalImage.getRGB(i, j) == 0xff000000)
						finalImage.setRGB(i, j, 0xffFFFFFF);
					else
						finalImage.setRGB(i, j, 0xff000000);
				}
			}		
			ui.setFinalImage(finalImage);	

			//Imagem de onde iremos salvar as fatias do molde
			sliceCrop.setImage(finalImage);
			
		}
	}
	
	
	ImagePlus mirrorBase;
	
	public void mirror(){
		if(sliceCrop == null) return;
		/*
		mirror = new ImagePlus("Mirror",sliceCrop.getImage());
		IJ.run(mirror, "Flip Horizontally", "");
		mirror.show();
		*/
	}

	public void brightnessAndcontrast(int bright, int contrast){

		BufferedImage src = deepCopy(getSlice());
		
		RescaleOp rescaleOp = new RescaleOp( ( (float)contrast/100) , bright, null);
		rescaleOp.filter(src,src);
		
		if(debug == null)
			debug = new ImagePlus("Ajuste da Imagem (não aplicado)", src);
		
		debug.setImage(src);
		debug.show();
	}
	
	static BufferedImage deepCopy(BufferedImage bi) {
		 ColorModel cm = bi.getColorModel();
		 boolean isAlphaPremultiplied = cm.isAlphaPremultiplied();
		 WritableRaster raster = bi.copyData(null);
		 return new BufferedImage(cm, raster, isAlphaPremultiplied, null);
	}

	public void applyToAllLayers() {
		log("Não implementado ainda");
	}

	public void applyToCurrentLayer() {
		
		//ci.imagePlus.getStack().g .getBufferedImage().createGraphics().drawString("Feito", 50, 50);
		
		//ci.imagePlus.getStack().setSliceLabel("FEITO", ci.imagePlus.getSlice());
		/*BufferedImage bi = getSlice();
		Graphics g = bi.createGraphics();
		g.setColor(Color.GREEN);
		g.drawString("Feito", 150, 150);
		setSlice(bi);
		*/

		sliceCrop.setImage( ((CustomPaintLabel) ui.finalLabel).getImage() );
		
		int border = 10;
		ImagePlus ip = sliceCrop.duplicate();
		IJ.run(ip, "Canvas Size...", "width="+(ip.getWidth()+border)+" height="+(ip.getHeight()+border)+" position=Center");
		//IJ.run(ip, "Make Binary", "calculate"); 
		//IJ.run(ip, "Invert", "");
		
		// Result Tree - to 3D 
		resultTree.put(ci.imagePlus.getSlice(), ip);
//		resultTree.put(ci.imagePlus.getSlice(), new ImagePlus(System.currentTimeMillis()+"", sliceCrop.getProcessor()));
		
		try {
			//System.out.println("Salvando imagem: resources/vitor/out_"+ci.imagePlus.getSlice()+".png");
			
			ImageIO.write(ip.getBufferedImage(), "png", new File("resources/vitor/out_"+ci.imagePlus.getSlice()+".png"));
		} catch (IOException e) {
			e.printStackTrace();
		}		
		
		
		//Next
		ci.imagePlus.setSlice(ci.imagePlus.getSlice()+1);
		setLastLayer();
	}
	
	private void scale(ImagePlus imp, boolean finalImage){
		
		if(true) return;
		
		//Resize do Canvas
		int height = imp.getHeight()+30;		
		
		if(finalImage){
			int width = imp.getWidth()+15;
			
			//Deixando a borda toda pra um dos lados apenas
			if(roi.getX() <= getSlice().getWidth()/2)
				IJ.run(imp, "Canvas Size...", "width="+width+" height="+height+" position=Center-Right");
			else
				IJ.run(imp, "Canvas Size...", "width="+width+" height="+height+" position=Center-Left");
		}else{
			int width = imp.getWidth()+30;
			IJ.run(imp, "Canvas Size...", "width="+width+" height="+height+" position=Center");
		}
		
	}
	
	public void snake(){
		
		//if(snakeGUI == null)
		snakeGUI = new SnakeGUI(this);
		
		try {
			snakeGUI.loadimage( ((CustomPaintLabel) ui.finalLabel).getImage() );
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void reload3d(){
		
		int w = resultTree.get(resultTree.firstKey()).getWidth();
		int h = resultTree.get(resultTree.firstKey()).getHeight();
		int l = resultTree.size() + 2; //head + tail
		
		// Criando o object3d, com os tamanhos e escalas baseadas nas propriedades do DICOM
		object3d = new Object3D(w, h, l);
		object3d.scaleX = ci.pixelSpacing/2;//ci.pixelSpacing / 2; //  /2  porque estamos dobrando o tamanho da imagem final???
		object3d.scaleY = ci.pixelSpacing/2;//ci.pixelSpacing / 2;
		object3d.scaleZ = ci.sliceSpacing;//ci.sliceThickness * resultTree.size();
		
		
		//Criando uma imagem em branco para ser a primeira e ultima camadas
		ImagePlus headAndTail = new ImagePlus("head", resultTree.get(resultTree.firstKey()).getProcessor());
		IJ.run(headAndTail, "Select All", "");
		IJ.run(headAndTail, "Clear", "slice");
		
		ImageStack stack3d = new ImageStack(w,h);
		stack3d.addSlice(headAndTail.getProcessor());
		//add head
		object3d.addImage(headAndTail.getBufferedImage());

		//add todas as outras
		for(Integer key : resultTree.keySet()){
			stack3d.addSlice(resultTree.get(key).getProcessor());
			object3d.addImage(resultTree.get(key).getBufferedImage());
		}
		
		//add tail
		object3d.addImage(headAndTail.getBufferedImage());
		stack3d.addSlice(headAndTail.getProcessor());
		
		//ImagePlus ip3d = new ImagePlus("3D",stack3d);
		//ip3d.show();
		
		/**
		 * Criando o Viewer
		 */
		if(viewer3d == null){
			viewer3d = new Viewer3D(object3d);
			viewer3d.setup();
		}else{
			viewer3d.reload();
		}
		object3d.saveSTL("/Users/emoretto/workspace.mestrado/earmaker/resources/", "out");
		
	}
	
	public void create3d(){
		
		/*
		 * 3D
		 */
		// Result TreeMap to ImageStack 
		
		System.out.println("sliceCrop.getWidth()" + sliceCrop.getWidth());
		
		ImageStack is = new ImageStack(sliceCrop.getWidth()*2, sliceCrop.getHeight()*2);
		
		for(Integer key : resultTree.keySet()){
			is.addSlice(key+"", resultTree.get(key).getProcessor());
		}
		
		ImagePlus resultStack = new ImagePlus("Slices final", is);
		
		new StackConverter(resultStack).convertToGray8();
		resultStack.show();
		
		WindowManager.setCurrentWindow(resultStack.getWindow());

		IJ.run("Make Binary", "calculate"); 
		
		//BinaryProcessor proc = new BinaryProcessor(new ByteProcessor(resultStack);
	
		Calibration cal = resultStack.getCalibration();

		// ISSO!! Distancia entre camadas!!
		//Main.console("slice thickness: "+DicomTools.getTag(dicomInfo, "0018,0050"));
		//Main.console("spacing between slices: "+DicomTools.getTag(dicomInfo, "0018,0088"));
		
		// Teste pra ver se veio os atributos de slice no DICOM
		if(DicomTools.getTag(ci.dicom, "0018,0050") == null && DicomTools.getTag(ci.dicom, "0018,0088") == null){
			console("Erro: DICOM sem informação de slice thickness (0018,0050) e spacing between slices (0018,0088)");
			cal.pixelDepth = 2;
			//return;
			
		// se veio só o slice thickness
		}else if( DicomTools.getTag(ci.dicom, "0018,0050") != null && DicomTools.getTag(ci.dicom, "0018,0088") == null)
			cal.pixelDepth = Float.parseFloat(DicomTools.getTag(ci.dicom, "0018,0050"));		
		else
			cal.pixelDepth = Float.parseFloat(DicomTools.getTag(ci.dicom, "0018,0050")) + Float.parseFloat(DicomTools.getTag(ci.dicom, "0018,0088"));
				
		cal.xOrigin -= cal.pixelWidth;
		cal.yOrigin -= cal.pixelHeight;		
		cal.zOrigin -= cal.pixelDepth;

		resultStack.setCalibration(cal);
		
				
		new ImageJ3DViewer().run("");
		//Surface 3D
//		new SurfacePlot2(resultStack).start();

	}
	
	public void finish(BufferedImage img){
		//sliceCrop.setImage(img);
		ui.setFinalImage(img,false);		
	}

	public boolean isSubtractiveMode() {
		return subtractiveMode;
	}

	public void setSubtractiveMode(boolean subtractiveMode) {
		this.subtractiveMode = subtractiveMode;
	}

}
