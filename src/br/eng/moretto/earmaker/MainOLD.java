package br.eng.moretto.earmaker;

import ij.IJ;
import ij.ImagePlus;
import ij.plugin.ContrastEnhancer;
import ij.plugin.frame.ContrastAdjuster;
import ij.process.ImageProcessor;

import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JOptionPane;

import br.eng.moretto.earmaker.dicom.PixelMed;
import br.eng.moretto.earmaker.regiongrow.QueueGrow;


/**
 * 
 * Reconstrucao 3d pelo ImageJ
 * 
 * Smooth no blender ou MeshLab
 * 
 * Corte com plano no Blender
 * https://www.youtube.com/watch?v=y6t9_pOmXCc
 * 
 * Usar o ReplicatorG só pra colocar a peça na base
 * 
 * TODO
 * Ajustar o Z da impressora em 0.01
 * REGION GROWING em 3D
 * Começar a imprimir peças
 * [HOLD] ver alguma convoluçao para tentar tratar aquelas linhas: http://homepages.inf.ed.ac.uk/rbf/HIPR2/linedet.htm
 * 
 * 
 * @author emoretto
 *
 */

public class MainOLD {

	static CurrentImage ci = null;
	
	protected Integer startSlice = -1;
	protected Integer endSlice = -1;
	protected Integer currentSlice = 0;
	protected Rectangle roi;
	
	
	public int threshold = 70;
	ImagePlus slice;
	ImagePlus sliceCrop;
	
	public MainOLD() {

		
		//Opening DICOM
		try{
			//ci = DicomOpener.run();
			ci = PixelMed.run();
		}catch(Exception e){ JOptionPane.showMessageDialog(null, "Não foi possível abrir a imagem"); e.printStackTrace(); return; }
		
		//Show
		ci.imagePlus.show();
		
		ci.imagePlus.getWindow().addMouseListener(new MouseListener() {
			
			@Override
			public void mouseReleased(MouseEvent e) {
				System.out.println(e.getX() +","+ e.getY() );		
			}
			
			@Override
			public void mousePressed(MouseEvent e) {
				System.out.println(e.getX() +","+ e.getY() );				
			}
			
			@Override
			public void mouseExited(MouseEvent e) {
			}
			
			@Override
			public void mouseEntered(MouseEvent e) {
			}
			
			@Override
			public void mouseClicked(MouseEvent e) {
				System.out.println(e.getX() +","+ e.getY() );		
				
			}
		});
		
		//Graphic User Inteface
		//new GUI(this);
	}
	
	protected void save(){
		
		ImagePlus ii = new ImagePlus("Slice", ci.imagePlus.getStack().getProcessor(currentSlice));	

		
		System.out.println("Slice saved: "+ci.imagePlus.getSlice());
	}	
	
	protected void startSelection(){
	
		startSlice = ci.imagePlus.getSlice();
		System.out.println("Slice Inicial: "+ci.imagePlus.getSlice());
		currentSlice = startSlice;
	}	
	
	protected void endSelection(){
		
		endSlice = ci.imagePlus.getSlice();
		System.out.println("Slice Final: "+ci.imagePlus.getSlice());
		System.out.println("Caixa de seleção: "+
				ci.imagePlus.getRoi().getBounds().getX()
				+","+ci.imagePlus.getRoi().getBounds().getY()
				+","+ci.imagePlus.getRoi().getBounds().getWidth()
				+","+ci.imagePlus.getRoi().getBounds().getHeight());

		roi = ci.imagePlus.getRoi().getBounds();
	}
	
	public static void main(String[] args) {
		new MainOLD();		
	}

	ImagePlus sliceSnake;
	
	public void next() {

		System.out.println("Processing slice #"+currentSlice);
		
		//Get slice
		slice = new ImagePlus("Slice", ci.imagePlus.getStack().getProcessor(currentSlice));		
		
		//Crop slice with ROI
		slice.setRoi(roi);
		//slice.getProcessor().fillOutside(new Roi(roi));
		
		//Crop slice with ROI
		ImageProcessor proc = slice.getProcessor().crop();
		if(sliceCrop == null){
			//proc.s .scale(2, 2);
			//proc.convolve3x3(new int[]{-1,2,-1,-1,2,-1,-1,2,-1});
			sliceCrop = new ImagePlus("Crop", proc);
		}else{
			sliceCrop.setProcessor(proc);
		}
		
		//Threshold
		
		// Otsu fail
		//Otsu otsu = new Otsu(sliceCrop.getProcessor());
		//sliceCrop.setProcessor(otsu.otsu());
		
		// Hard!!
		//IJ.run(sliceCrop,"8-bit","");
		
		setThreshold(threshold);
		
		/*
		// Image do Snake
		BufferedImage buffCrop = new BufferedImage(sliceCrop.getWidth()*2, sliceCrop.getHeight()*2, BufferedImage.TYPE_3BYTE_BGR);
		
		for(int i = 0 ; i < sliceCrop.getWidth() ; i++){
			for(int j = 0 ; j < sliceCrop.getHeight() ; j++){
				if(sliceCrop.getBufferedImage().getRGB(i, j) == 0xffffff00)
					buffCrop.setRGB(i+sliceCrop.getWidth()/2, j+sliceCrop.getHeight()/2, sliceCrop.getBufferedImage().getRGB(i, j));
			}
		}
		
		if(sliceSnake == null){
			sliceSnake = new ImagePlus("Snake",buffCrop);
			sliceSnake.show();
		}else
			sliceSnake.setProcessor(new ColorProcessor(buffCrop));
		
		sliceSnake.updateAndDraw();
		*/
		
		//Snake
		/*SnakeGUI snake = new SnakeGUI(this);
		
		try {
			snake.loadimage(buffCrop);
		} catch (IOException e) {
			e.printStackTrace();
		}*/
		
		
		//IJ.run(sliceCrop,"16-bit","");
		
//		IJ.setThreshold(0, 20);

		/*IJ.run(slice,"8-bit","");
		IJ.setThreshold(0, 20);
		*/
		//QueueGrow qg = new QueueGrow(sliceCrop.getProcessor(),20,0,0);
		
		//sliceCrop.show();
		
		/*
		//Borderss
		for(int j=3 ; j >= 0 ; j--)
			for(int i=0 ; i < sliceCrop.getWidth() ; i++){
				sliceCrop.getProcessor().putPixel(i, j, 0);
				sliceCrop.getProcessor().putPixel(i, sliceCrop.getHeight()-j-1, 0);
			}
			
			for(int j=3 ; j >= 0 ; j--)
			for(int i=0 ; i < sliceCrop.getHeight() ; i++){
				sliceCrop.getProcessor().putPixel(j, i, 0);
				sliceCrop.getProcessor().putPixel(sliceCrop.getWidth()-1-j, i, 0);
			}
		
		//Snake
		SnakeGUI snake = new SnakeGUI(this);
		
		try {
			snake.loadimage(sliceCrop.getBufferedImage());
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		*/
		
		sliceCrop.updateAndDraw();
		currentSlice++;
		
	}
	
	public void finish(BufferedImage img){
		
	}
	
	/** 
	 * Where the magic happens!
	 */
	public void start() {

		currentSlice = startSlice;
		
		while(currentSlice < endSlice){
		
			System.out.println(currentSlice);
			next();
			
			
			// Image do Snake
			BufferedImage buffCrop = new BufferedImage(sliceCrop.getWidth(), sliceCrop.getHeight(), BufferedImage.TYPE_3BYTE_BGR);
			
			for(int i = 0 ; i < sliceCrop.getWidth() ; i++){
				for(int j = 0 ; j < sliceCrop.getHeight() ; j++){
					if(sliceCrop.getBufferedImage().getRGB(i, j) == 0xffffff00)
						buffCrop.setRGB(i, j, sliceCrop.getBufferedImage().getRGB(i, j));
				}
			}
			
			try {
				ImageIO.write(buffCrop, "png", new File("resources/out/out_"+currentSlice+".png"));
			} catch (IOException e) {
				e.printStackTrace();
			}
			
		}
				
	}
	
	
	public void setThreshold(int value) {

		threshold = value;
		System.out.println("Threshold "+value);
		
		//sliceCrop.setProcessor(slice.getProcessor().crop());
		
		// Hard!!
		IJ.run(sliceCrop,"8-bit","");
		IJ.run(sliceCrop,"RGB Color","");
		
		QueueGrow qg = new QueueGrow(sliceCrop.getProcessor(),threshold,0,sliceCrop.getWidth());
		
		if(!sliceCrop.isVisible())
			sliceCrop.show();
		else
			sliceCrop.updateAndDraw();
				
				
		if(true) return;
		
		slice = new ImagePlus("Slice", ci.imagePlus.getStack().getProcessor(startSlice));		
		
		//32768,35540
		
		for(int i=0 ; i < slice.getWidth() ; i++){
			for(int j=0 ; j < slice.getHeight() ; j++){
				if(slice.getProcessor().getPixel(i, j)-31744 < value)
					slice.getProcessor().putPixel(i, j, 32768);
			}	
		}

		if(!slice.isVisible())
			slice.show();
	}

	public void setContrast(int value) {
		
		ImagePlus slice = new ImagePlus("Slice", ci.imagePlus.getStack().getProcessor(currentSlice));	
		ContrastAdjuster ca = new ContrastAdjuster();
		
		ContrastEnhancer enh = new ContrastEnhancer();
		enh.stretchHistogram(slice, value);
		slice.updateAndDraw();
		
	}	
}
