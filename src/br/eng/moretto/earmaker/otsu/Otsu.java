package br.eng.moretto.earmaker.otsu;

import ij.process.ImageProcessor;

public class Otsu {

	ImageProcessor ip;
	int min = 9999999;
	int max = -9999999;
	
	public Otsu(ImageProcessor ip) {
		this.ip = ip;		
	}
	
	// compute histogram of pixel array.
	public int[] computeHist() {
		
		for(int i = 0 ; i < ip.getWidth() ; i++){
			for(int j = 0 ; j < ip.getHeight() ; j++){
				if(ip.getPixelValue(i, j) > max)
					max = ip.getPixel(i, j);
				if(ip.getPixelValue(i, j) < min)
					min = ip.getPixel(i, j);
			}
		}
		System.out.println(min+","+max);
			
		int[] histogram = new int[max-min+1];

		for (int i = 0; i <= max-min; i++) { histogram[i] = 0; }
		
		for (int k = 0; k <= max-min; k++) {
			for(int i = 0 ; i < ip.getWidth() ; i++){
				for(int j = 0 ; j < ip.getHeight() ; j++){
					if ( ip.getPixel(i, j)-min == i ) {
						histogram[k]+=1;
					}
				}
			}
		}
		return histogram;

	} // end computeHist
		
	public ImageProcessor otsu() {
		
			int[] data = computeHist();
			
            // Otsu's threshold algorithm
            // C++ code by Jordan Bevik <Jordan.Bevic@qtiworld.com>
            // ported to ImageJ plugin by G.Landini
            int k,kStar;  // k = the current threshold; kStar = optimal threshold
            double N1, N;    // N1 = # points with intensity <=k; N = total number of points
            double BCV, BCVmax; // The current Between Class Variance and maximum BCV
            double num, denom;  // temporary bookeeping
            double Sk;  // The total intensity for all histogram points <=k
            double S;//, L=256; // The total intensity of the image
            int L = max-min;
            
            // Initialize values:
            S = N = 0;
            for (k=0; k<L; k++){
                    S += (double) k * data[k];       // Total histogram intensity
                    N += data[k];           // Total number of data points
            }

            Sk = 0;
            N1 = data[0]; // The entry for zero intensity
            BCV = 0;
            BCVmax=0;
            kStar = 0;

            // Look at each possible threshold value,
            // calculate the between-class variance, and decide if it's a max
            for (k=1; k<L-1; k++) { // No need to check endpoints k = 0 or k = L-1
                    Sk += (double)k * data[k];
                    N1 += data[k];

                    // The float casting here is to avoid compiler warning about loss of precision and
                    // will prevent overflow in the case of large saturated images
                    denom = (double)( N1) * (N - N1); // Maximum value of denom is (N^2)/4 =  approx. 3E10

                    if (denom != 0 ){
                            // Float here is to avoid loss of precision when dividing
                            num = ( (double)N1 / N ) * S - Sk;      // Maximum value of num =  255*N = approx 8E7
                            BCV = (num * num) / denom;
                    }
                    else
                            BCV = 0;

                    if (BCV >= BCVmax){ // Assign the best threshold found so far
                            BCVmax = BCV;
                            kStar = k;
                    }
            }
            // kStar += 1;  // Use QTI convention that intensity -> 1 if intensity >= k
            // (the algorithm was developed for I-> 1 if I <= k.)
            
            for(int i = 0 ; i < ip.getWidth() ; i++){
				for(int j = 0 ; j < ip.getHeight() ; j++){
					if ( ip.getPixel(i, j)-min >= kStar ) {
						ip.putPixel(i, j, max);
					}else
						ip.putPixel(i, j, min);
				}
			}
            
            return ip;
            //return kStar+min;
    }
}

