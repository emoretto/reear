/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.eng.moretto.earmaker;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.awt.image.BufferedImage;

import javax.swing.JPanel;

/**
 *
 * @author emoretto
 */
public class CustomPaintLabel extends JPanel {
    
    private BufferedImage image; //original
    
    private BufferedImage canvasImage; //canvas
    
    Point startDrag;
        
    boolean black = true;
    
    boolean brush = true;
    
    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);        
        g.drawImage(canvasImage, 0, 0, null); // see javadoc for more info on the parameters            
    }
    
    public BufferedImage getImage(){    	
    	return canvasImage;
    }
    
    public void resetImage(){    	
    	setImage(this.image,true);
    }
        
    public void setImage(BufferedImage image){
    	setImage(image,true);
    }
    
    public void setImage(BufferedImage image, boolean scale){
    	
        //original
        this.image = image;
        
        //Scale x2
        BufferedImage buffered = null;
        if(scale){
        	Image tmp = image.getScaledInstance(image.getWidth()*2, image.getHeight()*2, BufferedImage.SCALE_SMOOTH);
        	buffered = new BufferedImage(image.getWidth()*2,image.getHeight()*2,BufferedImage.TYPE_INT_RGB);
        	buffered.getGraphics().drawImage(tmp, 0, 0, null);
        }else{
        	buffered = new BufferedImage(image.getWidth(),image.getHeight(),BufferedImage.TYPE_INT_RGB);
        	buffered.getGraphics().drawImage(image, 0, 0, null);
        }   
        this.canvasImage = buffered;
        repaint();
    }
    
    public void toggleColor(){
    	this.black = !black;
    }
    
     public void toggleBrushLine(){
    	this.brush = !brush;
    }
    
     public void draw(Point point) {
        Graphics2D g = this.canvasImage.createGraphics();
        //g.setRenderingHints(renderingHints);
        if(black)
        	g.setColor(Color.black);
        else
        	g.setColor(Color.white);
        
        
        
        if(brush){
            g.setStroke(new BasicStroke(UI.strokeSize,BasicStroke.CAP_ROUND,BasicStroke.JOIN_ROUND,1.0f));
            int n = (int) UI.strokeSize/2;
        
            g.drawOval(point.x-n, point.y-n, n, n);
            g.fillOval(point.x-n, point.y-n, n, n);      
        }else{
             //Line draw
             //g.setColor(Color.WHITE);
             g.drawLine(startDrag.x, startDrag.y, point.x, point.y);
        }
        
        g.dispose();
        //this.imageLabel.repaint();
        this.repaint();
    }
     
    CustomPaintLabel(){
        
        this.addMouseListener(new MouseAdapter() {
            public void mousePressed(MouseEvent e) {
                
                startDrag = new Point(e.getX(), e.getY());
                repaint();
            }

            public void mouseReleased(MouseEvent e) {
            
              //Line
              draw(e.getPoint());
              repaint();
            }
          });

          this.addMouseMotionListener(new MouseMotionAdapter() {
            public void mouseDragged(MouseEvent e) {
             
                if(brush)
                    draw(e.getPoint());                
            }
          });

    }
}
