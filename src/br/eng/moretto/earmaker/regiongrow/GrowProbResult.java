package br.eng.moretto.earmaker.regiongrow;

import java.awt.image.BufferedImage;

public class GrowProbResult{
	
	public GrowProbResult(BufferedImage buffRet, BufferedImage buffRetFinal) {
		this.resultAnalysis = buffRet;
		this.result = buffRetFinal;
	}
	
	public BufferedImage resultAnalysis;
	public BufferedImage result;
	
}