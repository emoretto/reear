package br.eng.moretto.earmaker.regiongrow;

import java.awt.image.BufferedImage;
import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;

public class QueueGrowDICOM {

	private static final int _FILL_COLOR = 0xFFFFFFFF;
	BufferedImage buff;	
	BufferedImage buffRet;
	boolean map[][];
	int threshold;

	public QueueGrowDICOM(BufferedImage buff, int threshold) {
		super();
		this.buff = buff;
		this.threshold = threshold;
		map = new boolean[buff.getWidth()][buff.getHeight()];		
		buffRet = new BufferedImage(buff.getWidth(), buff.getHeight(), BufferedImage.TYPE_3BYTE_BGR);
	}
	
	int getPixel(int x, int y){		
    	int[] storedPixelValues = buff.getRaster().getPixel(x,y, new int[3]);
    	return storedPixelValues[0];
	}
	
	public BufferedImage grow(){ 
		
		// procurando a primeira posicao de fundo da imagem
		int li = 0;
		int ci = buff.getWidth()-1;
		/*
		for(int i=0 ; i< buff.getWidth() ; i++){
			for(int j=0 ; j< buff.getHeight() ; j++){
				if(getPixel(i,j) <= threshold){
					li = j;
					ci = i;
					j = 999999; // saida do for
					i = 999999; // saida do for
				}					
			}
		}
		*/
		//agora sim, balde de tinta
	  Queue<Integer> fila = new LinkedBlockingQueue<Integer>();
	  fila.add(li); fila.add(ci);

	  while (fila.size() > 0) {
		  
		int y=fila.poll();
	    int x=fila.poll();

	    try{
	    	
		    if ( getPixel(x,y) <= threshold && map[x][y] == false ) {
		    	
		    	map[x][y] = true;
		    	buffRet.setRGB(x, y, _FILL_COLOR);
	
		    	if(x+1 < buff.getWidth() && getPixel(x+1, y )  <= threshold ){
		    		fila.add(y); fila.add(x + 1); // direita
		    	}
		    	if(y+1 < buff.getHeight() && getPixel(x, y+1)  <= threshold){
		    		fila.add(y + 1); fila.add(x); // baixo
		    	}
		    	if(x >= 1  && getPixel(x-1, y)  <= threshold){
		    		fila.add(y); fila.add(x-1); // esquerda
		    	}
		    	if(y >= 1 && getPixel(x, y-1)  <= threshold){				
		    		fila.add(y-1); fila.add(x);   // acima
		    	}
		    }
		    
	    }catch (Exception e){
	    	System.err.println(x+","+y);
	    }
	  }
	  
	  return buffRet;
	}
	
	
}
