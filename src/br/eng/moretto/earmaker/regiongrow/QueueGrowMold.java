package br.eng.moretto.earmaker.regiongrow;

import ij.ImagePlus;

import java.awt.image.BufferedImage;
import java.util.Queue;
import java.util.TreeMap;
import java.util.concurrent.LinkedBlockingQueue;

import br.eng.moretto.earmaker.CurrentImage;

public class QueueGrowMold {

	private static final int _LOWER_THRESHOLD = 50;
	
	private static final int _FAIL_FILL_COLOR = 0xFFCCCCCC;
	
	private static final int _SUCCESS_FILL_COLOR = 0xFFFFFFFF;

	private static final boolean debug = false;

	private TreeMap<Integer, Particle> particles = new TreeMap<Integer, Particle>();
	private BufferedImage buff;	
	private BufferedImage buffRet;
	private BufferedImage buffRetFinal;
	private CurrentImage currentImage;
	private boolean map[][];
	private int threshold;

	public QueueGrowMold( BufferedImage buff, CurrentImage currentImage, int threshold ) {
		this.buff = buff;
		this.currentImage = currentImage;
		this.threshold = threshold;
		map = new boolean[buff.getWidth()][buff.getHeight()];		
		buffRet = new BufferedImage(buff.getWidth(), buff.getHeight(), BufferedImage.TYPE_3BYTE_BGR);
		buffRetFinal = new BufferedImage(buff.getWidth(), buff.getHeight(), BufferedImage.TYPE_3BYTE_BGR);
	}
	
	int getPixel(int x, int y){		
    	int[] storedPixelValues = buff.getRaster().getPixel(x,y, new int[3]);
    	//return (int) (storedPixelValues[0] * currentImage.getRescaleSlope() + currentImage.getRescaleIntercept());
    	return (int) (storedPixelValues[0]);
	}
	
	private boolean compare(int x1,int y1, int x, int y, BufferedImage buff){
		
		if(x1 < 0 || x < 0 || y1 < 0 || y1 < 0)
			return false;
		
		if(x1 == buff.getWidth() || y1 == buff.getHeight() || x == buff.getWidth() || y == buff.getHeight())
			return false;
		
		if(Math.abs(buff.getRGB(x1,y1) - buff.getRGB(x, y)) < threshold)	
			return true;
		else 
			return false;
	}
	
	private boolean compareDICOM(int x1,int y1, int x, int y, BufferedImage buff){
		
		if(x1 < 0 || x < 0 || y1 < 0 || y1 < 0)
			return false;
		
		if(x1 == buff.getWidth() || y1 == buff.getHeight() || x == buff.getWidth() || y == buff.getHeight())
			return false;
		System.out.println(getPixel(x1,y1));
		System.out.println(getPixel(x, y));
		if(Math.abs(getPixel(x1,y1) - getPixel(x, y)) < threshold+20)	
			return true;
		else 
			return false;
	}
	
	
	public GrowProbResult grow(){ 
	
	  Queue<Integer> fila = new LinkedBlockingQueue<Integer>();
	  
	  System.out.println("IMG SIZE: "+ buff.getWidth() + "x"+buff.getHeight());
	  
	  ImagePlus ip = null;
	  
	  if(debug){
		  ip = new ImagePlus("CRP", buffRet);
		  ip.show();	  
		  try { Thread.sleep(100);  } catch (InterruptedException e1) {  }
	  }
		
	  for( int i = buff.getWidth()-1 ; i >= 1 ; i--){
		  
		  for( int j = 1 ; j < buff.getHeight()-1 ; j++){
			
				if(map[i][j] == false ){
					fila.add(i); fila.add(j);
			  	}
	  
			  	int qtdPixel = 0;
			  	int intensityTotal = 0;
				int maxX = 0;
				int minX = 99999;
				int maxY = 0;
				int minY = 99999;
				int startX = -1;
				int startY = -1;
				
				while (fila.size() > 0) {

					int x = fila.poll();
					int y = fila.poll();
				
					if(startX == -1) startX = x;
					if(startY == -1) startY = y;
					try {
						
						//intensityTotal += getPixel(x, y);
						qtdPixel ++;
						
						int t = (255 << 24) | (50 << 16) | (50 << 8) | 50;
						if ( map[x][y] == false  && getPixel(x,y) <= _LOWER_THRESHOLD) {

							map[x][y] = true;

							//int c2 = (255 << 24) | (threshold << 16) | (threshold << 8) | (int) Math.round(Math.random()*10);
							buffRet.setRGB(x, y, 0x00FF0000);

							if (compare(x+1,y,x,y,buff)) {
								fila.add(x + 1); // direita
								fila.add(y);
								if(x+1 > maxX) maxX = x+1;
							}
							if (compare(x,y+1,x,y,buff)) {
								fila.add(x); // baixo
								fila.add(y + 1);
								if(y+1 > maxY) maxY = y+1;
							}
							if (compare(x-1,y,x,y,buff)) {
								fila.add(x - 1); // esquerda
								fila.add(y);
								if(x-1 < minX) minX = x-1;
							}
							if (compare(x,y-1,x,y,buff)) {
								fila.add(x); // acima
								fila.add(y - 1);
								if(y-1 < minY) minY = y-1;
							}
							
							//diagonais
							if (compare(x-1,y-1,x,y,buff)) {
								fila.add(x - 1); fila.add(y - 1);
								if(x-1 < minX) minX = x-1;
								if(y-1 < minY) minY = y-1;
							}
							
							if (compare(x+1,y+1,x,y,buff)) {
								fila.add(x + 1); fila.add(y + 1);
								if(x+1 > maxX) maxX = x+1;
								if(y+1 > maxY) maxY = y+1;
							}

							if (compare(x-1,y+1,x,y,buff)) {
								fila.add(x - 1); fila.add(y + 1);
								if(x-1 < minX) minX = x-1;
								if(y+1 > maxY) maxY = y+1;
							}

							if (compare(x+1,y-1,x,y,buff)) {
								fila.add(x + 1); fila.add(y - 1);
								if(x+1 > maxX) maxX = x+1;
								if(y-1 < minY) minY = y-1;
							}
						}

					} catch (Exception e) {
						e.printStackTrace();
						System.out.println("ERRO"+ x + "," + y);
					}
				}	
				
				if(minX != 99999 && minY != 99999){
					
					if(debug){
						ip.setImage(buffRet);
						ip.updateAndDraw();

						try {
							Thread.sleep(1);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					}
					
					Particle p = new Particle(qtdPixel, intensityTotal, (maxX-minX), (maxY-minY), minX, minY, maxX, maxY, startX, startY);
					System.out.println(p);//"STAT: qd largura="+ (maxX-minX) +" altura="+(maxY-minY)+" --- "+minX+","+maxX+" - "+minY+","+maxY);
					particles.put(p.qtdPixel, p);
				}				
		  }
	  }

	  
	  System.out.println("**************** CLASSIFICADOR ******************");
	 /*
	  //Getting the bigger one
	  int key = particles.lastKey();
	  Particle pOne = particles.remove(key);
	  fill(pOne.startX, pOne.startY, 0x00FFFFFF);
	  System.out.println("Filling: "+pOne.toString());*/
	  
	  //New version
	  //getting all the bigger ones
	  for(Integer key : particles.keySet()){
		  Particle pOne = particles.get(key);
		  
		  if(pOne.qtdPixel > 1000){
			  fill(pOne.startX, pOne.startY, 0x00FFFFFF);
			  System.out.println("Filling: "+pOne.toString());
		  }
	  }
	 
	  
	  return new GrowProbResult(buffRet, buffRetFinal);
	}

	private void fill(int i, int j, int color){
		
		boolean map[][] = new boolean[buff.getWidth()][buff.getHeight()];	
		Queue<Integer> fila = new LinkedBlockingQueue<Integer>();
		
		fila.add(i); fila.add(j);		
		
		while (fila.size() > 0) {

			int x = fila.poll(); int y = fila.poll();
		
			try {
				
				if ( map[x][y] == false ){

					map[x][y] = true;
					buffRetFinal.setRGB(x, y, color);

					if (compare(x+1,y,x,y,buffRet)) {
						fila.add(x + 1); // direita
						fila.add(y);
					}
					if (compare(x,y+1,x,y,buffRet)) {
						fila.add(x); // baixo
						fila.add(y + 1);
					}
					if (compare(x-1,y,x,y,buffRet)) {
						fila.add(x - 1); // esquerda
						fila.add(y);
					}
					if (compare(x,y-1,x,y,buffRet)) {
						fila.add(x); // acima
						fila.add(y - 1);
					}
					
					//diagonais
					if (compare(x-1,y-1,x,y,buffRet)) {
						fila.add(x - 1); fila.add(y - 1);
					}
					if (compare(x+1,y+1,x,y,buffRet)) {
						fila.add(x + 1); fila.add(y + 1);
					}
					if (compare(x-1,y+1,x,y,buffRet)) {
						fila.add(x - 1); fila.add(y + 1);
					}
					if (compare(x+1,y-1,x,y,buffRet)) {
						fila.add(x + 1); fila.add(y - 1);
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
				System.out.println("ERRO "+ x + "," + y);
			}
		}	
	}
}
