package br.eng.moretto.earmaker.regiongrow;

import ij.process.ImageProcessor;

import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;

public class QueueGrow {

	private static final int _FILL_COLOR = 0xffffFF00;
	ImageProcessor buff;
	int threshold;
	int li;
	int ci;

	public QueueGrow(ImageProcessor buff, int threshold, int li, int ci) {
		super();
		this.buff = buff;
		this.threshold = threshold;
		this.li = li;
		this.ci = ci;
		grow();
	}

	void grow()
	{ 
	  Queue<Integer> fila = new LinkedBlockingQueue<Integer>();
	  fila.add(li); fila.add(ci);

	  int t = (255 << 24) | (threshold << 16) | (threshold << 8) | threshold;
	  
	  
	  
	  while (fila.size() > 0) {
		  
		int y=fila.poll();
	    int x=fila.poll();

	    try{
		    if ( buff.getPixel(x,y) << 8 >= 0 && buff.getPixel(x,y) <= t) {
		    	
		    	buff.putPixel(x,y, _FILL_COLOR);
	
		    	if(x+1 < buff.getWidth()-1 && buff.getPixel(x+1, y )  <= t ){
		    		fila.add(y); fila.add(x + 1); // direita
		    	}
		    	if(y+1 < buff.getHeight()-1 && buff.getPixel(x, y+1)  <= t){
		    		fila.add(y + 1); fila.add(x); // baixo
		    	}
		    	if(x > 1  && buff.getPixel(x-1, y)  <= t){
		    		fila.add(y); fila.add(x-1); // esquerda
		    	}
		    	if(y > 1 && buff.getPixel(x, y-1)  <= t){				
		    		fila.add(y-1); fila.add(x);   // acima
		    	}
		    }
		    
	    }catch (Exception e){
	    	System.err.println(x+","+y);
	    }
	  }
	}
	
	
}
