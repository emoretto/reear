package br.eng.moretto.earmaker.regiongrow;

public class Particle {

	int qtdPixel;
	int width;
	int height;
	int minX;
	int minY;
	int maxX;
	int maxY;
	int startX;
	int startY;
	
	float avgIntensity;

	public Particle(int qtdPixel, int intensityTotal, int width, int height, int minX, int minY, int maxX, int maxY, int startX, int startY) {
		this.qtdPixel = qtdPixel;
		this.width = width;
		this.height = height;
		this.minX = minX;
		this.minY = minY;
		this.maxX = maxX;
		this.maxY = maxY;
		this.startX = startX;
		this.startY = startY;
		
		avgIntensity = ( (float) intensityTotal / (float) qtdPixel) ;
	}

	@Override
	public String toString() {
		return "Particle [qtdPixel=" + qtdPixel + ", width=" + width
				+ ", height=" + height + ", minX=" + minX + ", minY=" + minY
				+ ", maxX=" + maxX + ", maxY=" + maxY + ", avgIntensity="
				+ avgIntensity + "]";
	}

	
}
