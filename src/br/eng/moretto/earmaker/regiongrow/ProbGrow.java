package br.eng.moretto.earmaker.regiongrow;

import ij.IJ;
import ij.ImagePlus;
import ij.ImageStack;
import ij.process.ImageConverter;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.TreeMap;

import br.eng.moretto.earmaker.CurrentImage;
import br.eng.moretto.earmaker.dicom.GantryTiltCorrection;

import com.pixelmed.dicom.AttributeList;
import com.pixelmed.dicom.DicomException;
import com.pixelmed.dicom.DicomInputStream;
import com.pixelmed.dicom.TagFromName;
import com.pixelmed.display.SourceImage;

public class ProbGrow {

	
	public static void main(String[] args) throws IOException, DicomException {
		
		
//		DicomInputStream i = new DicomInputStream(new File("/Users/emoretto/workspace.mestrado/earmaker/resources/DICOM/S467300/S20/I270"));
		DicomInputStream i = new DicomInputStream(new File("/Users/emoretto/workspace.mestrado/earmaker/resources/kss_dicom2/Kaique_Da_Silva_Santos/IM-0001-0050.dcm"));
//		DicomInputStream i = new DicomInputStream(new File("/Users/emoretto/workspace.mestrado/earmaker/IM-0001-0023.dcm"));
		
		AttributeList list = new AttributeList();
		list.read(i);				

		//  hu = pixel_value * slope + intercept
		System.out.println("RescaleIntercept " + list.get(TagFromName.RescaleIntercept).getFloatValues()[0]);
		System.out.println("RescaleSlope " + list.get(TagFromName.RescaleSlope).getFloatValues()[0]);

		final float rescaleIntercept = list.get(TagFromName.RescaleIntercept).getFloatValues()[0];
		final float rescaleSlope = list.get(TagFromName.RescaleSlope).getFloatValues()[0];
		
		SourceImage sImg = new SourceImage(list);	
		
		
		final BufferedImage buff = sImg.getBufferedImage().getSubimage(300, 210, 150, 180); //KSS
//		final BufferedImage buff = sImg.getBufferedImage().getSubimage(355, 150, 150, 180); //AFR
		
		
		//BufferedImage buff = ImageIO.read(new File("/Users/emoretto/workspace.mestrado/earmaker/I260-iter20_c.tif"));
		
		ImagePlus ip2 = new ImagePlus("img1",buff);
		ip2.show();
		ip2.getWindow().getCanvas().addMouseListener(new MouseListener() {
			@Override public void mouseReleased(MouseEvent e) {		}
			@Override public void mousePressed(MouseEvent e) {		}
			@Override public void mouseExited(MouseEvent e) {		}
			@Override public void mouseEntered(MouseEvent e) {		}
			@Override public void mouseClicked(MouseEvent e) {
				int[] storedPixelValues = buff.getRaster().getPixel(e.getX(),e.getY(), new int[3]);
				System.out.println("("+e.getX()+","+e.getY()+"): "+ (storedPixelValues[0] * rescaleSlope + rescaleIntercept) );
			}
		});

		//burocracia
		ImageStack istack = new ImageStack(buff.getWidth(), buff.getHeight());
		istack.addSlice("0", ip2.getProcessor());	
		ImagePlus ipStack = new ImagePlus("Slices DICOM", istack);
		
		CurrentImage ci = new CurrentImage();
		ci.imagePlus = ipStack;
		ci.setDicomInfo = new TreeMap<Integer, AttributeList>();
		ci.setDicomInfo.put(0, list);
		// eof burocracia		
		
		
		QueueGrowProb qgp = new QueueGrowProb(buff, ci, 200,-200,400);
		GrowProbResult res = qgp.grow();
		

		final ImagePlus ip = new ImagePlus("img",res.result);
		ip.show();
		
	
		ip.getWindow().getCanvas().addMouseListener(new MouseListener() {
			@Override public void mouseReleased(MouseEvent e) {		}
			@Override public void mousePressed(MouseEvent e) {		}
			@Override public void mouseExited(MouseEvent e) {		}
			@Override public void mouseEntered(MouseEvent e) {		}
			@Override public void mouseClicked(MouseEvent e) {
				int[] storedPixelValues = ip.getBufferedImage().getRaster().getPixel(e.getX(),e.getY(), new int[3]);
				System.out.println("IP ("+e.getX()+","+e.getY()+"): "+ (storedPixelValues[0] * rescaleSlope + rescaleIntercept) );
				System.out.println("IP ("+e.getX()+","+e.getY()+"): "+ (storedPixelValues[0]) );
				int t = (255 << 24) | (50 << 16) | (50 << 8) | 50;
				System.out.println("IP ("+e.getX()+","+e.getY()+"): "+ buff.getRGB(e.getX(), e.getY()) );
			}
		});
		
//		
//		new ImageConverter(ip).convertToGray8();
//		IJ.run("Make Binary", "calculate"); 
//		
		
		QueueGrowMold qgpMold = new QueueGrowMold(res.result, ci, 1);
		GrowProbResult resMold = qgpMold.grow();
		
		
		
		//ImagePlus ip = new ImagePlus("img",res.resultAnalysis);
		//ip.show();
		
		ImagePlus ipFinal = new ImagePlus("Mold",resMold.result);
		ipFinal.show();
		
		GantryTiltCorrection gtc = new GantryTiltCorrection();
		gtc.transform(resMold.result, ci);

		
	}
}
