package br.eng.moretto.earmaker.regiongrow;

import ij.ImagePlus;

import java.awt.image.BufferedImage;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;

import br.eng.moretto.earmaker.CurrentImage;

public class QueueGrowProb {

	// menor valor a ser considerado regiao de interesse
	//private static final int _LOWER_THRESHOLD = -200;
	
	private static final int _BG_FILL_COLOR = 0xFF000000;
	private static final int _SUCCESS_FILL_COLOR = 0xFFFFFFFF;

//	private static final int _BG_FILL_COLOR = 0xFFFFFFFF;
//	private static final int _SUCCESS_FILL_COLOR = 0xFF000000;

	
	private static final boolean debug = false;

	private List<Particle> particles = new LinkedList<Particle>();
	private BufferedImage buff;	
	private BufferedImage buffRet;
	private BufferedImage buffRetFinal;
	private CurrentImage currentImage;
	private int colorIncrement = 100;
	private boolean map[][];
	//threshold para pertencer ao mesmo grupo
	private int differenceThreshold = 400 ;
	private int lowerThreshold = -200 ;
	
	private int threshold;

	public QueueGrowProb( BufferedImage buff, CurrentImage currentImage, int threshold, int lowerThreshold, int differenceThreshold) {
		this.buff = buff;
		this.currentImage = currentImage;
		this.threshold = threshold;
		this.differenceThreshold = differenceThreshold;
		this.lowerThreshold = lowerThreshold;
		
		map = new boolean[buff.getWidth()][buff.getHeight()];		
		buffRet = new BufferedImage(buff.getWidth(), buff.getHeight(), BufferedImage.TYPE_3BYTE_BGR);
		buffRetFinal = new BufferedImage(buff.getWidth(), buff.getHeight(), BufferedImage.TYPE_3BYTE_BGR);
	}
	
	int getPixel(int x, int y){		
    	int[] storedPixelValues = buff.getRaster().getPixel(x,y, new int[3]);
    	return (int) (storedPixelValues[0] * currentImage.getRescaleSlope() + currentImage.getRescaleIntercept());
	}
	
	private boolean compare(int x1,int y1, int x, int y){
		
		if(x1 < 0 || x < 0 || y1 < 0 || y1 < 0)
			return false;
		
		if(x1 == buff.getWidth() || y1 == buff.getHeight() || x == buff.getWidth() || y == buff.getHeight())
			return false;
		
		if(Math.abs(getPixel(x1,y1) - getPixel(x, y)) < differenceThreshold)		
			return true;
		else 
			return false;
	}
	
	
	public GrowProbResult grow(){ 
	
	  Queue<Integer> fila = new LinkedBlockingQueue<Integer>();
	  
	  System.out.println("IMG SIZE: "+ buff.getWidth() + "x"+buff.getHeight());
	  
	  ImagePlus ip = null;
	  
	  if(debug){
		  ip = new ImagePlus("CRP", buffRet);
		  ip.show();	  
		  try { Thread.sleep(100);  } catch (InterruptedException e1) {  }
	  }
		
	  for( int i = buff.getWidth()-1 ; i >= 1 ; i--){
		  
		  for( int j = 1 ; j < buff.getHeight()-1 ; j++){
			
				if(map[i][j] == false ){
					fila.add(i); fila.add(j);
			  	}
	  
			  	int qtdPixel = 0;
			  	int intensityTotal = 0;
				int maxX = 0;
				int minX = 99999;
				int maxY = 0;
				int minY = 99999;
				int startX = -1;
				int startY = -1;
				
				while (fila.size() > 0) {

					int x = fila.poll();
					int y = fila.poll();
				
					if(startX == -1) startX = x;
					if(startY == -1) startY = y;
					try {
						
						intensityTotal += getPixel(x, y);
						qtdPixel ++;
						
						if ( map[x][y] == false &&  getPixel(x,y) > lowerThreshold) {

							map[x][y] = true;

							int c = (255 << 24) | (255 << 16) | (255 << 8) | colorIncrement;
							buffRet.setRGB(x, y, c);

							if (compare(x+1,y,x,y)) {
								fila.add(x + 1); // direita
								fila.add(y);
								if(x+1 > maxX) maxX = x+1;
							}
							if (compare(x,y+1,x,y)) {
								fila.add(x); // baixo
								fila.add(y + 1);
								if(y+1 > maxY) maxY = y+1;
							}
							if (compare(x-1,y,x,y)) {
								fila.add(x - 1); // esquerda
								fila.add(y);
								if(x-1 < minX) minX = x-1;
							}
							if (compare(x,y-1,x,y)) {
								fila.add(x); // acima
								fila.add(y - 1);
								if(y-1 < minY) minY = y-1;
							}
							
							//diagonais
							if (compare(x-1,y-1,x,y)) {
								fila.add(x - 1); fila.add(y - 1);
								if(x-1 < minX) minX = x-1;
								if(y-1 < minY) minY = y-1;
							}
							
							if (compare(x+1,y+1,x,y)) {
								fila.add(x + 1); fila.add(y + 1);
								if(x+1 > maxX) maxX = x+1;
								if(y+1 > maxY) maxY = y+1;
							}

							if (compare(x-1,y+1,x,y)) {
								fila.add(x - 1); fila.add(y + 1);
								if(x-1 < minX) minX = x-1;
								if(y+1 > maxY) maxY = y+1;
							}

							if (compare(x+1,y-1,x,y)) {
								fila.add(x + 1); fila.add(y - 1);
								if(x+1 > maxX) maxX = x+1;
								if(y-1 < minY) minY = y-1;
							}
						}

					} catch (Exception e) {
						e.printStackTrace();
						System.out.println("ERRO"+ x + "," + y);
					}
				}	
				
				if(minX != 99999 && minY != 99999){
					
					if(debug){
						ip.setImage(buffRet);
						ip.updateAndDraw();

						try {
							Thread.sleep(1);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					}
					
					Particle p = new Particle(qtdPixel, intensityTotal, (maxX-minX), (maxY-minY), minX, minY, maxX, maxY, startX, startY);
					//System.out.println(p);//"STAT: qd largura="+ (maxX-minX) +" altura="+(maxY-minY)+" --- "+minX+","+maxX+" - "+minY+","+maxY);
					particles.add(p);
				}				
				colorIncrement+=10;
		  }
	  }

	  
	  System.out.println("**************** CLASSIFICADOR ******************");
	  /*
		Substance	HU
		Air	−1000
		Lung	−500
		Fat	−100 to −50
		Water	0
		CSF	15
		Kidney	30
		Blood	+30 to +45
		Muscle	+10 to +40
		Grey matter	+37 to +45
		White matter	+20 to +30
		Liver	+40 to +60
		Soft Tissue, Contrast	+100 to +300
		Bone	+700 (cancellous bone) to +3000 (dense bone)
	   */
	  //final String CSI = "\u001b[";
	  
	  for(Particle p : particles){
		  
		  // IF FAIL!?
		  if(p.width < 5 || p.qtdPixel < 100 || p.avgIntensity < threshold){

			  //if(p.qtdPixel > 100) //dropando informacoes de ruidos granulares
				  System.out.println("😡👎🏽 "+ p.toString());
			  
			  //fill(p.startX,p.startY,_FAIL_FILL_COLOR);
			  fill(p.startX,p.startY,_BG_FILL_COLOR);

			  /*			  
			  int red = (int) Math.round(Math.random()*6);
			  int green = (int) Math.round(Math.random()*6);
			  int blue = (int) Math.round(Math.random()*6);
			  int color = 16 + (red * 36) + (green * 6) + blue;
              System.out.printf(CSI + "48;5;%dm   ", color);*/
            
		  // ELSE SUCCESS!!
		  }else{
			  fill(p.startX,p.startY,_SUCCESS_FILL_COLOR);
			  System.out.println("😀👍 ok "+ p.toString());
		  }
		  
		  if(debug){ 
			  ip.setImage(buffRetFinal);
			  ip.updateAndDraw();
			  try { Thread.sleep(10);  } catch (InterruptedException e) { }
	  	  }
	  }	  
	    
	  
	  return new GrowProbResult(buffRet, buffRetFinal);
	}

	private void fill(int i, int j, int color){
		
		boolean map[][] = new boolean[buff.getWidth()][buff.getHeight()];	
		Queue<Integer> fila = new LinkedBlockingQueue<Integer>();
		
		fila.add(i); fila.add(j);		
		
		while (fila.size() > 0) {

			int x = fila.poll(); int y = fila.poll();
		
			try {
				
				if ( map[x][y] == false &&  getPixel(x,y) > lowerThreshold){

					map[x][y] = true;
					buffRetFinal.setRGB(x, y, color);

					if (compare(x+1,y,x,y)) {
						fila.add(x + 1); // direita
						fila.add(y);
					}
					if (compare(x,y+1,x,y)) {
						fila.add(x); // baixo
						fila.add(y + 1);
					}
					if (compare(x-1,y,x,y)) {
						fila.add(x - 1); // esquerda
						fila.add(y);
					}
					if (compare(x,y-1,x,y)) {
						fila.add(x); // acima
						fila.add(y - 1);
					}
					
					//diagonais
					if (compare(x-1,y-1,x,y)) {
						fila.add(x - 1); fila.add(y - 1);
					}
					if (compare(x+1,y+1,x,y)) {
						fila.add(x + 1); fila.add(y + 1);
					}
					if (compare(x-1,y+1,x,y)) {
						fila.add(x - 1); fila.add(y + 1);
					}
					if (compare(x+1,y-1,x,y)) {
						fila.add(x + 1); fila.add(y - 1);
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
				System.out.println("ERRO "+ x + "," + y);
			}
		}	
	}
	
	
}
