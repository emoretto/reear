package br.eng.moretto.earmaker;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;


public class Interpolate {

	private static final int _FILL_COLOR = 0xffffff00;
	private static final int _FORE_COLOR = 0xff000000;
	
	public static void main(String[] args) throws Exception {
		
		String inputDir = "/Users/emoretto/workspace.mestrado/earmaker/resources/kss_out";
		
		File dir = new File(inputDir);
				
		int count = 0;

		
		File[] files = dir.listFiles();
		for(int i=1 ; i < files.length ; i++){
						
			System.out.println(files[i-1].getName()+  " = "+ files[i].getName());
			
			BufferedImage buff0 = ImageIO.read(files[i-1]);
			BufferedImage buff1 = ImageIO.read(files[i-1]);
			BufferedImage buffOut = new BufferedImage(buff0.getWidth(), buff0.getHeight(), BufferedImage.TYPE_3BYTE_BGR);
			
			int pos0=0;
			int pos1=0;
			
			for(int y=0 ; y < buff0.getHeight() ; y++){
				for(int x=0 ; x < buff0.getWidth() ; x++){
					
					buffOut.setRGB(x, y, _FILL_COLOR);
					
					if(buff0.getRGB(x, y) == _FILL_COLOR){
						pos0 = x;
					}

					if(buff1.getRGB(x, y) == _FILL_COLOR){
						pos1 = x;
					}
					
									
					
					
					
					buffOut.setRGB(x, y, _FORE_COLOR);
					
				}
				
				//output
				for(int x=0 ; x < pos0 ; x++){
					buffOut.setRGB(x, y, _FILL_COLOR);
				}
				//break;
			}
			
			ImageIO.write(buffOut, "png", new File("/Users/emoretto/workspace.mestrado/earmaker/resources/kss_out_interp/interp_"+count+".png"));
			
			break;			
			
		}
		
	}
	
}
